
module.exports = (lib, game, ui, get, ai, _status) => {
    const methods = (isInit) => {
        lib.storage["rExtension_StandardLib"] = true;
        if (!lib.rExtension) lib.rExtension = {};
        lib.rExtension["StandardLib"] = {
            bool: true
        };
        // Class Setting
        {
            /*
            class Standard {
                static overrides(dest, src) {
                    if (!dest._super) dest._super = {};
                    for (let key in src) {
                        if (dest[key])
                            dest._super[key] = dest[key];

                        dest[key] = src[key];
                    }
                    return true;
                }
            }
            */
            const Standard = {
                overrides: function (dest, src) {
                    if (!dest._super) dest._super = {};
                    for (let key in src) {
                        if (dest[key])
                            dest._super[key] = dest[key];

                        dest[key] = src[key];
                    }
                    return true;
                },
                path: function (...paths) {
                    const platform = process.platform;
                    const pat = platform == "win32" ? "\\" : "/"
                    return `${lib.assetURL}.${pat}/${paths.join(pat)}`;
                },
                refresh: function () {
                    lib.standard = this;
                }
            }

            if (!lib.standard) lib.standard = Standard;
            if (!lib.rExtension["StandardLib"].lib) lib.rExtension["StandardLib"].lib = Standard;
            // To Do
        }
        // Get Extraen
        const newGet = () => {
            let newGet = {
                randomSkills: function (num) {
                    let skills = [];
                    for (let name in lib.character) {
                        let character = lib.character[name];
                        for (let skill of character[3]) {
                            let info = get.info(skill);
                            if (info) skills.add(skill);
                        }
                    }
                    return skills.randomGets(num);
                },
                cardDiscard: function (num) {
                    if (_status.waitingForCards) {
                        ui.create.cards.apply(ui.create, _status.waitingForCards);
                        delete _status.waitingForCards;
                    }
                    var list = Array.new();
                    var card = false;
                    if (typeof num != "number") num = 1;
                    if (num == 0) { card = true; num = 1; }
                    if (num < 0) num = 1;
                    while (num--) {
                        if (ui.discardPile.hasChildNodes() == false) {
                            break;
                        }
                        var cardx = ui.discardPile.removeChild(ui.discardPile.lastChild);
                        cardx.original = "c";
                        list.push(cardx);
                    }
                    game.updateRoundNumber();
                    if (card) return list[0];
                    return list;
                },
                cardDiscardBottom: function (num) {
                    if (_status.waitingForCards) {
                        ui.create.cards.apply(ui.create, _status.waitingForCards);
                        delete _status.waitingForCards;
                    }
                    var list = Array.new();
                    var card = false;
                    if (typeof num != "number") num = 1;
                    if (num == 0) {
                        card = true;
                        num = 1;
                    }
                    if (num < 0) num = 1;
                    while (num--) {
                        if (ui.discardPile.hasChildNodes() == false) {
                            break;
                        }
                        var cardx = ui.discardPile.removeChild(ui.discardPile.firstChild);
                        cardx.original = "c";
                        list.push(cardx);
                    }
                    game.updateRoundNumber();
                    if (card) return list[0];
                    return list;
                },
                number: function (card, player) {
                    if (get.itemtype(card) === "cards" && card.length === 1) return get.number(card[0], player);
                    else if (get.itemtype(card.cards) == "cards" && card.cards.length === 1) return get.number(card.cards[0], player);
                    else if (typeof card.number === "number") {
                        if (get.itemtype(card) === "card") {
                            let owner = player || get.owner(card);
                            if (owner) {
                                return game.checkMod(card, card.number, "number", owner);
                            }
                        }
                        return card.number;
                    }
                    return null;
                },
                type: function (obj, method, player) {
                    if (typeof obj == "string") obj = { name: obj };
                    if (typeof obj != "object") return;
                    var name = get.name(obj, player);
                    if (!lib.card[name]) return;
                    let type = lib.card[name].type;
                    if (!player) player = get.owner(obj);
                    if (player) type = game.checkMod(obj, type, "type", player);
                    if (method == "trick" && type == "delay") return "trick";
                    return type;
                }
            };

            lib.standard.overrides(get, newGet);
        };
        // Player Methods
        const newPlayer = () => {
            lib.element.player.changeHpTo = function (num) {
                let next = game.createEvent("changeHpTo");
                next.player = this;
                if (!num) num = this.hp;
                event.num = num;
                next.setContent("changeHpTo");
                return next;
            };

            lib.element.player.changeCardTo = function (num) {
                let next = game.createEvent("changeCardTo");
                next.player = this;
                if (!num) num = this.countCards("h");
                event.num = num;
                next.setContent("changeCardTo");
                return next;
            };

            lib.element.player.choosePlayerDisable = function (target, isDoubleHorse) {
                let next = game.createEvent("choosePlayerDisable");
                next.player = this;
                next.target = (target) ? target : this;
                if (isDoubleHorse) next.set("horse", true);
                next.setContent("choosePlayerDisable");
                return next;
            };

            lib.element.player.chooseToRemove = function (isForced) {
                if (!isForced) isForced = false;
                return this.choosePlayerRemove(this, isForced);
            };

            lib.element.player.choosePlayerRemove = function (target, isForced) {
                let next = game.createEvent("choosePlayerRemove");
                next.player = this;
                next.target = (target) ? target : this;
                next.filter = lib.filter.all;
                if (isForced) next.set("forced", true);
                next.setContent("choosePlayerRemove");
            };

            lib.element.player.setStorage = function (name, value) {
                this.storage[name] = value;
                return 0;
            };

            lib.element.player.updateStorage = function (name, func) {
                let result = func(this.storage[name]);
                this.storage[name] = result ? result : undefined;
                return 0;
            };

            lib.element.player.countExpansions = function (skill) {
                return this.getExpansions(skill).length;
            };

            lib.element.player.checkGroupSkill = function (skill, group, disable) {
                if (this.group == group) {
                    return true;
                }
                else {
                    if (disable !== false) {
                        this.awakenSkill(skill);
                    }
                    return false;
                }
            };

            lib.element.player.checkZhuSkill = function (skill, group, disable) {
                if (this.isZhu) {
                    return true;
                }
                else {
                    if (disable !== false) {
                        this.awakenSkill(skill);
                    }
                    return false;
                }
            };

            lib.element.player.loseToCardpile = function () {
                var next = game.createEvent("loseToCardpile");
                next.player = this;
                next.num = 0;
                for (var i = 0; i < arguments.length; i++) {
                    if (get.itemtype(arguments[i]) == "player") {
                        next.source = arguments[i];
                    }
                    else if (get.itemtype(arguments[i]) == "cards") {
                        next.cards = arguments[i].slice(0);
                    }
                    else if (get.itemtype(arguments[i]) == "card") {
                        next.cards = [arguments[i]];
                    }
                    else if (typeof arguments[i] == "boolean") {
                        next.animate = arguments[i];
                    }
                    else if (get.objtype(arguments[i]) == "div") {
                        next.position = arguments[i];
                    }
                    else if (arguments[i] == "notBySelf") {
                        next.notBySelf = true;
                    }
                }
                if (next.cards == undefined) _status.event.next.remove(next);
                next.setContent("loseToCardpile");
                return next;
            },

                /*
                lib.element.player.damageMultiple = function (target, isForced) {
                    let next = game.createEvent("choosePlayerRemove");
                    next.player = this;
                    next.target = (target) ? target : this;
                    next.filter = lib.filter.all;
                    if (isForced) next.set("forced", true);
                    next.setContent("choosePlayerRemove");
                }
                */

                lib.element.player.drawAndDiscard = function (num1, num2, ...options) {
                    let next = game.createEvent("drawAndDiscard");
                    next.player = this;
                    next.num1 = (num1) ? num1 : 1;
                    next.num2 = (num2) ? num2 : 1;
                    for (let option of options) {
                        switch (option) {
                            case "bottom":
                                next.bottom = true;
                                break;
                            case "discard":
                                next.discard = true;
                                break;
                            case "nodelay":
                                next.nodelay = true;
                                break;
                        }
                    }
                    next.setContent("drawAndDiscard");
                    return next;
                };

            lib.element.player.hasCards = function () {
                return this.countCards(...arguments) > 0;
            };
        }
        // Element Content
        const newElement = () => {
            lib.element.content.changeHpTo = function () {
                let hp = player.hp;
                if (event.num === hp) return;
                player.changeHp(event.num - hp);
            }

            lib.element.content.changeCardTo = function () {
                let num = player.countCards("h");
                if (event.num == num) return;
                else if (event.num < num) player.chooseToDiscard(num - event.num, true, "h");
                else if (event.num > num) player.draw(event.num - num);
            }

            lib.element.content.choosePlayerDisable = function () {
                "step 0"
                let list = Array.seq(1, event.horse ? 6 : 5).filter(i => {
                    if ([3, 4].includes(i) && event.horse) return false;
                    // if (i === 6 && !event.horse) return false;
                    if (target.isDisabled(i)) return false;
                    return true;
                }).map(item => `equip${item}`);
                if (list.isEmpty()) event.finish();
                else {
                    event.list = list;
                    var next = player.chooseControl(list);
                    next.set("prompt", "请选择废除一个装备栏");
                    if (!event.ai) event.ai = (event, player, list) => {
                        return list.randomGet();
                    }
                    event.ai = event.ai(event.getParent(), player, list);
                    next.ai = () => {
                        return event.ai;
                    };
                }
                "step 1"
                if (event.logSkill) player.logSkill(event.logSkill, target);
                event.result = { bool: true, control: result.control };
                if (result.control === "equip6") {
                    target.disableEquip(3);
                    target.disableEquip(4);
                    return;
                }
                target.disableEquip(result.control);
            };

            lib.element.content.choosePlayerRemove = function () {
                "step 0"
                let skills = target.skills.filter(event.filter);
                if (!event.forced) skills.push("cancel2");
                player.chooseControl(skills).set("prompt", "选择要失去的技能").set("ai", () => {
                    return skills.randomGet();
                }).set("prompt2", skills.filter(skill => skill != "cancel2").map(skill => `【${get.translation(skill)}】${get.translation(`${skill}_info`)}`).join("</br></br>"));
                "step 1"
                if (result.control == "cancel2") {
                    event.result = { bool: false };
                    event.finish();
                }
                event.result = { bool: true, control: result.control };
                if (event.logSkill) {
                    let options = Array.new();
                    if (target != player) options.push(target);
                    player.logSkill(event.logSkill, ...options);
                };
                player.removeSkill(result.control);
                if (!event.quiet) target.popup(result.control);
                if (!event.quiet) game.log(target, "失去了", "【" + get.skillTranslation(result.control, player) + "】");
            }

            lib.element.content.drawAndDiscard = function () {
                "step 0"
                let next = player.draw(event.num1);
                if (event.bottom) next.set("bottom", true);
                if (event.discard) next.set("discard", true);
                if (event.nodelay) next.set("nodelay", true);
                "step 1"
                player.chooseToDiscard(event.num2, true, "he");
            };

            lib.element.content.chooseDiscardDamage = function () {
                'step 0'
                if (player.isHealthy() && event.forced) {
                    player.draw(event.num1);
                    event.finish();
                    return;
                }
                var controls = ['draw_card'];
                if (player.isDamaged()) {
                    event.num2 = Math.min(event.num2, player.maxHp - player.hp);
                    controls.push('recover_hp');
                }
                if (!event.forced) {
                    controls.push('cancel2');
                }
                var prompt = event.prompt;
                if (!prompt) {
                    if (player.isHealthy()) {
                        prompt = '是否摸' + get.cnNumber(event.num1) + '张牌？';
                    }
                    else {
                        prompt = '摸' + get.cnNumber(event.num1) + '张牌或回复' + get.cnNumber(event.num2) + '点体力';
                    }
                }
                var next = player.chooseControl(controls);
                next.set('prompt', prompt);
                if (event.hsskill) next.setHiddenSkill(event.hsskill);
                if (event.ai) {
                    next.set('ai', event.ai);
                }
                else {
                    var choice;
                    if (player.isDamaged() && get.recoverEffect(player) > 0 && (
                        player.hp == 1 || player.needsToDiscard() ||
                        player.hasSkillTag('maixie_hp') || event.num2 > event.num1 ||
                        (event.num2 == event.num1 && player.needsToDiscard(1))
                    )) {
                        choice = 'recover_hp';
                    }
                    else {
                        choice = 'draw_card';
                    }
                    next.set('ai', function () {
                        return _status.event.choice;
                    });
                    next.set('choice', choice);
                }
                'step 1'
                if (result.control != 'cancel2') {
                    if (event.logSkill) {
                        if (typeof event.logSkill == 'string') {
                            player.logSkill(event.logSkill);
                        }
                        else if (Array.isArray(event.logSkill)) {
                            player.logSkill.apply(player, event.logSkill);
                        }
                    }
                    if (result.control == 'draw_card') {
                        player.draw(event.num1);
                    }
                    else {
                        player.recover(event.num2);
                    }
                }
                event.result = result;
            };

            lib.element.content.loseToCardpile = function () {
                "step 0"
                game.log(player, '将', cards, '置入了弃牌堆');
                event.done = player.lose(cards, event.position, 'visible');
                if (!event.bottom) event.done.insert_card = true;
                event.done.type = 'loseToCardpile';
                "step 1"
                event.trigger('loseToCardpile');
            };
        };
        // Game MutilPlayers
        const mutilPlayers = () => {
            var style1 = document.createElement("style");
            style1.innerHTML += "[data-number=\"9\"]>.player[data-position=\"1\"]{top:45px;left:calc(97% - 75px);}";
            style1.innerHTML += "[data-number=\"9\"]>.player[data-position=\"2\"]{top:30px;left:calc(84% - 75px);}";
            style1.innerHTML += "[data-number=\"9\"]>.player[data-position=\"3\"]{top:15px;left:calc(71% - 75px);}";
            style1.innerHTML += "[data-number=\"9\"]>.player[data-position=\"4\"]{top:0px;left:calc(58% - 75px);}";
            style1.innerHTML += "[data-number=\"9\"]>.player[data-position=\"5\"]{top:0px;left:calc(45% - 75px);}";
            style1.innerHTML += "[data-number=\"9\"]>.player[data-position=\"6\"]{top:15px;left:calc(32% - 75px);}";
            style1.innerHTML += "[data-number=\"9\"]>.player[data-position=\"7\"]{top:30px;left:calc(19% - 75px);}";
            style1.innerHTML += "[data-number=\"9\"]>.player[data-position=\"8\"]{top:45px;left:calc(6% - 75px);}";

            style1.innerHTML += "[data-number=\"10\"]>.player[data-position=\"1\"]{top:calc(200% / 3 - 190px);left:calc(97% - 75px);}";
            style1.innerHTML += "[data-number=\"10\"]>.player[data-position=\"2\"]{top:calc(100% / 3 - 190px);left:calc(97% - 75px);}";
            style1.innerHTML += "[data-number=\"10\"]>.player[data-position=\"3\"]{top:0px;left:calc(82% - 75px);}";
            style1.innerHTML += "[data-number=\"10\"]>.player[data-position=\"4\"]{top:0px;left:calc(67% - 75px);}";
            style1.innerHTML += "[data-number=\"10\"]>.player[data-position=\"5\"]{top:0px;left:calc(52% - 75px);}";
            style1.innerHTML += "[data-number=\"10\"]>.player[data-position=\"6\"]{top:0px;left:calc(37% - 75px);}";
            style1.innerHTML += "[data-number=\"10\"]>.player[data-position=\"7\"]{top:0px;left:calc(22% - 75px);}";
            style1.innerHTML += "[data-number=\"10\"]>.player[data-position=\"8\"]{top:calc(100% / 3 - 190px);left:calc(7% - 75px);}";
            style1.innerHTML += "[data-number=\"10\"]>.player[data-position=\"9\"]{top:calc(200% / 3 - 190px);left:calc(7% - 75px);}";

            style1.innerHTML += "[data-number=\"11\"]>.player[data-position=\"1\"]{top:calc(200% / 3 - 190px);left:calc(82% - 80px);}";
            style1.innerHTML += "[data-number=\"11\"]>.player[data-position=\"2\"]{top:calc(200% / 3 - 190px);left:calc(97% - 80px);}";
            style1.innerHTML += "[data-number=\"11\"]>.player[data-position=\"3\"]{top:calc(100% / 3 - 190px);left:calc(97% - 80px);}";
            style1.innerHTML += "[data-number=\"11\"]>.player[data-position=\"4\"]{top:0px;left:calc(82% - 75px);}";
            style1.innerHTML += "[data-number=\"11\"]>.player[data-position=\"5\"]{top:0px;left:calc(67% - 75px);}";
            style1.innerHTML += "[data-number=\"11\"]>.player[data-position=\"6\"]{top:0px;left:calc(52% - 75px);}";
            style1.innerHTML += "[data-number=\"11\"]>.player[data-position=\"7\"]{top:0px;left:calc(37% - 75px);}";
            style1.innerHTML += "[data-number=\"11\"]>.player[data-position=\"8\"]{top:0px;left:calc(22% - 75px);}";
            style1.innerHTML += "[data-number=\"11\"]>.player[data-position=\"9\"]{top:calc(100% / 3 - 190px);left:calc(6% - 65px);}";
            style1.innerHTML += "[data-number=\"11\"]>.player[data-position=\"10\"]{top:calc(200% / 3 - 190px);left:calc(6% - 65px);}";

            style1.innerHTML += "[data-number=\"12\"]>.player[data-position=\"1\"]{top:calc(200% / 3 - 190px);left:calc(82% - 80px);}";
            style1.innerHTML += "[data-number=\"12\"]>.player[data-position=\"2\"]{top:calc(200% / 3 - 190px);left:calc(97% - 80px);}";
            style1.innerHTML += "[data-number=\"12\"]>.player[data-position=\"3\"]{top:calc(100% / 3 - 190px);left:calc(97% - 80px);}";
            style1.innerHTML += "[data-number=\"12\"]>.player[data-position=\"4\"]{top:0px;left:calc(82% - 75px);}";
            style1.innerHTML += "[data-number=\"12\"]>.player[data-position=\"5\"]{top:0px;left:calc(67% - 75px);}";
            style1.innerHTML += "[data-number=\"12\"]>.player[data-position=\"6\"]{top:0px;left:calc(52% - 75px);}";
            style1.innerHTML += "[data-number=\"12\"]>.player[data-position=\"7\"]{top:0px;left:calc(37% - 75px);}";
            style1.innerHTML += "[data-number=\"12\"]>.player[data-position=\"8\"]{top:0px;left:calc(22% - 75px);}";
            style1.innerHTML += "[data-number=\"12\"]>.player[data-position=\"9\"]{top:calc(100% / 3 - 190px);left:calc(6% - 65px);}";
            style1.innerHTML += "[data-number=\"12\"]>.player[data-position=\"10\"]{top:calc(200% / 3 - 190px);left:calc(6% - 65px);}";
            style1.innerHTML += "[data-number=\"12\"]>.player[data-position=\"11\"]{top:calc(200% / 3 - 190px);left:calc(22% - 65px);}";

            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"1\"]{top:calc(200% / 3 - 190px);left:calc(84% - 80px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"2\"]{top:calc(200% / 3 - 190px);left:calc(97% - 80px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"3\"]{top:calc(100% / 3 - 190px);left:calc(97% - 80px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"4\"]{top:0px;left:calc(84% - 75px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"5\"]{top:0px;left:calc(71% - 75px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"6\"]{top:0px;left:calc(58% - 75px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"7\"]{top:0px;left:calc(45% - 75px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"8\"]{top:0px;left:calc(32% - 75px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"9\"]{top:0px;left:calc(19% - 75px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"10\"]{top:calc(100% / 3 - 190px);left:calc(6% - 65px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"11\"]{top:calc(200% / 3 - 190px);left:calc(6% - 65px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"12\"]{top:calc(200% / 3 - 190px);left:calc(6% - 65px);}";
            style1.innerHTML += "[data-number=\"13\"]>.player[data-position=\"11\"]{top:calc(200% / 3 - 190px);left:calc(19% - 65px);}";

            document.head.appendChild(style1);

            lib.translate.unknown8 = "九号位";
            lib.translate.unknown9 = "十号位";
            lib.translate.unknown10 = "十一号位";
            lib.translate.unknown11 = "十二号位";
            lib.translate.unknown12 = "十三号位";
            lib.mode.identity.config.player_number.item = {
                "2": "两人",
                "3": "三人",
                "4": "四人",
                "5": "五人",
                "6": "六人",
                "7": "七人",
                "8": "八人",
                "9": "九人",
                "10": "十人",
                "11": "十一人",
                "12": "十二人",
                "13": "十三人",
            }
            lib.mode.guozhan.config.player_number.item = {
                "2": "两人",
                "3": "三人",
                "4": "四人",
                "5": "五人",
                "6": "六人",
                "7": "七人",
                "8": "八人",
                "9": "九人",
                "10": "十人",
                "11": "十一人",
                "12": "十二人",
                "13": "十三人",
            };

            let identity = ["zhu", "zhong", "zhong", "zhong", "nei", "fan", "fan", "fan", "fan"];
            if (lib.config.mode_config.identity.identity[7] == undefined && get.mode() != "connect") lib.config.mode_config.identity.identity.push(identity);
            identity = ["zhu", "zhong", "zhong", "zhong", "nei", "fan", "fan", "fan", "fan", "fan"];
            if (lib.config.mode_config.identity.identity[8] == undefined && get.mode() != "connect") lib.config.mode_config.identity.identity.push(identity);
            identity = ["zhu", "zhong", "zhong", "zhong", "zhong", "nei", "fan", "fan", "fan", "fan", "fan"];
            if (lib.config.mode_config.identity.identity[9] == undefined && get.mode() != "connect") lib.config.mode_config.identity.identity.push(identity);
            identity = ["zhu", "zhong", "zhong", "zhong", "zhong", "nei", "fan", "fan", "fan", "fan", "fan", "fan"];
            if (lib.config.mode_config.identity.identity[10] == undefined && get.mode() != "connect") lib.config.mode_config.identity.identity.push(identity);
            identity = ["zhu", "zhong", "zhong", "zhong", "zhong", "nei", "fan", "fan", "fan", "fan", "fan", "fan", "fan"];
            if (lib.config.mode_config.identity.identity[11] == undefined && get.mode() != "connect") lib.config.mode_config.identity.identity.push(identity);
            identity = ["zhu", "zhong", "zhong", "zhong", "zhong", "zhong", "zhong", "nei", "fan", "fan", "fan", "fan", "fan", "fan"];
        };
        // Node Intro
        const nodeIntro = () => {
            get.nodeintro = function (node, simple, evt) {
                var uiintro = ui.create.dialog('hidden', 'notouchscroll');
                if (node.classList.contains('player') && !node.name) {
                    return uiintro;
                }
                var i, translation, intro, str;
                if (node._nointro) return;
                if (typeof node._customintro == 'function') {
                    if (node._customintro(uiintro) === false) return;
                }
                else if (Array.isArray(node._customintro)) {
                    var caption = node._customintro[0];
                    var content = node._customintro[1];
                    if (typeof caption == 'function') {
                        caption = caption(node);
                    }
                    if (typeof content == 'function') {
                        content = content(node);
                    }
                    uiintro.add(caption);
                    uiintro.add('<div class="text center" style="padding-bottom:5px">' + content + '</div>');
                }
                else if (node.classList.contains('player') || node.linkplayer) {
                    if (node.linkplayer) {
                        node = node.link;
                    }
                    var capt = get.translation(node.name);
                    if ((lib.character[node.name] && lib.character[node.name][1]) || lib.group.contains(node.group)) {
                        capt += '&nbsp;&nbsp;' + (lib.group.contains(node.group) ? get.translation(node.group) : lib.translate[lib.character[node.name][1]]);
                    }
                    uiintro.add(capt);

                    if (lib.characterTitle[node.name]) {
                        uiintro.addText(get.colorspan(lib.characterTitle[node.name]));
                    }

                    if (!node.noclick && (node.isUnderControl() || (!game.observe && game.me && game.me.hasSkillTag('viewHandcard', null, node, true)))) {
                        var hs = node.getCards('h');
                        if (hs.length) {
                            uiintro.add('<div class="text center">手牌</div>');
                            uiintro.addSmall(node.getCards('h'));
                        }
                    }
                    // Visualened Card
                    if (!node.noclick && (game.me ? game.me != node : true) && node.hasCards(card => card.hasGaintag("visualCard"))) {
                        var hs = node.getCards("h", card => card.hasGaintag("visualCard"));
                        if (hs.length) {
                            uiintro.add('<div class="text center">明示牌</div>');
                            uiintro.addSmall(hs);
                        }
                    }

                    var skills = node.getSkills(null, null, false).slice(0);
                    var skills2 = game.filterSkills(skills, node);
                    if (node == game.me && node.hiddenSkills.length) {
                        skills.addArray(node.hiddenSkills);
                    }
                    for (var i in node.disabledSkills) {
                        if (node.disabledSkills[i].length == 1 &&
                            node.disabledSkills[i][0] == i + '_awake' &&
                            !node.hiddenSkills.contains(i)) {
                            skills.add(i);
                        }
                    }
                    for (i = 0; i < skills.length; i++) {
                        if (lib.skill[skills[i]] && (lib.skill[skills[i]].nopop || lib.skill[skills[i]].equipSkill)) continue;
                        if (lib.translate[skills[i] + '_info']) {
                            translation = lib.translate[skills[i] + '_ab'] || get.translation(skills[i]).slice(0, 2);
                            if (node.forbiddenSkills[skills[i]]) {
                                var forbidstr = '<div style="opacity:0.5"><div class="skill">【' + translation + '】</div><div>';
                                if (node.forbiddenSkills[skills[i]].length) {
                                    forbidstr += '（与' + get.translation(node.forbiddenSkills[skills[i]]) + '冲突）<br>';
                                }
                                else {
                                    forbidstr += '（双将禁用）<br>';
                                }
                                forbidstr += get.skillInfoTranslation(skills[i], node) + '</div></div>'
                                uiintro.add(forbidstr);
                            }
                            else if (!skills2.contains(skills[i])) {
                                if (lib.skill[skills[i]].preHidden && get.mode() == 'guozhan') {
                                    uiintro.add('<div><div class="skill" style="opacity:0.5">【' + translation + '】</div><div><span style="opacity:0.5">' + get.skillInfoTranslation(skills[i], node) + '</span><br><div class="underlinenode on gray" style="position:relative;padding-left:0;padding-top:7px">预亮技能</div></div></div>');
                                    var underlinenode = uiintro.content.lastChild.querySelector('.underlinenode');
                                    if (_status.prehidden_skills.contains(skills[i])) {
                                        underlinenode.classList.remove('on');
                                    }
                                    underlinenode.link = skills[i];
                                    underlinenode.listen(ui.click.hiddenskill);
                                }
                                else uiintro.add('<div style="opacity:0.5"><div class="skill">【' + translation + '】</div><div>' + get.skillInfoTranslation(skills[i], node) + '</div></div>');
                            }
                            else if (lib.skill[skills[i]].temp || !node.skills.contains(skills[i]) || lib.skill[skills[i]].thundertext) {
                                if (lib.skill[skills[i]].frequent || lib.skill[skills[i]].subfrequent) {
                                    uiintro.add('<div><div class="skill thundertext thunderauto">【' + translation + '】</div><div class="thundertext thunderauto">' + get.skillInfoTranslation(skills[i], node) + '<br><div class="underlinenode on gray" style="position:relative;padding-left:0;padding-top:7px">自动发动</div></div></div>');
                                    var underlinenode = uiintro.content.lastChild.querySelector('.underlinenode');
                                    if (lib.skill[skills[i]].frequent) {
                                        if (lib.config.autoskilllist.contains(skills[i])) {
                                            underlinenode.classList.remove('on');
                                        }
                                    }
                                    if (lib.skill[skills[i]].subfrequent) {
                                        for (var j = 0; j < lib.skill[skills[i]].subfrequent.length; j++) {
                                            if (lib.config.autoskilllist.contains(skills[i] + '_' + lib.skill[skills[i]].subfrequent[j])) {
                                                underlinenode.classList.remove('on');
                                            }
                                        }
                                    }
                                    if (lib.config.autoskilllist.contains(skills[i])) {
                                        underlinenode.classList.remove('on');
                                    }
                                    underlinenode.link = skills[i];
                                    underlinenode.listen(ui.click.autoskill2);
                                }
                                else {
                                    uiintro.add('<div><div class="skill thundertext thunderauto">【' + translation + '】</div><div class="thundertext thunderauto">' + get.skillInfoTranslation(skills[i], node) + '</div></div>');
                                }
                            }
                            else if (lib.skill[skills[i]].frequent || lib.skill[skills[i]].subfrequent) {
                                uiintro.add('<div><div class="skill">【' + translation + '】</div><div>' + get.skillInfoTranslation(skills[i], node) + '<br><div class="underlinenode on gray" style="position:relative;padding-left:0;padding-top:7px">自动发动</div></div></div>');
                                var underlinenode = uiintro.content.lastChild.querySelector('.underlinenode');
                                if (lib.skill[skills[i]].frequent) {
                                    if (lib.config.autoskilllist.contains(skills[i])) {
                                        underlinenode.classList.remove('on');
                                    }
                                }
                                if (lib.skill[skills[i]].subfrequent) {
                                    for (var j = 0; j < lib.skill[skills[i]].subfrequent.length; j++) {
                                        if (lib.config.autoskilllist.contains(skills[i] + '_' + lib.skill[skills[i]].subfrequent[j])) {
                                            underlinenode.classList.remove('on');
                                        }
                                    }
                                }
                                if (lib.config.autoskilllist.contains(skills[i])) {
                                    underlinenode.classList.remove('on');
                                }
                                underlinenode.link = skills[i];
                                underlinenode.listen(ui.click.autoskill2);
                            }
                            else if (lib.skill[skills[i]].clickable && node.isIn() && node.isUnderControl(true)) {
                                var intronode = uiintro.add('<div><div class="skill">【' + translation + '】</div><div>' + get.skillInfoTranslation(skills[i], node) + '<br><div class="menubutton skillbutton" style="position:relative;margin-top:5px">点击发动</div></div></div>').querySelector('.skillbutton');
                                if (!_status.gameStarted || (lib.skill[skills[i]].clickableFilter && !lib.skill[skills[i]].clickableFilter(node))) {
                                    intronode.classList.add('disabled');
                                    intronode.style.opacity = 0.5;
                                }
                                else {
                                    intronode.link = node;
                                    intronode.func = lib.skill[skills[i]].clickable;
                                    intronode.classList.add('pointerdiv');
                                    intronode.listen(ui.click.skillbutton);
                                }
                            }
                            else if (lib.skill[skills[i]].nobracket) {
                                uiintro.add('<div><div class="skilln">' + get.translation(skills[i]) + '</div><div>' + lib.translate[skills[i] + '_info'] + '</div></div>');
                            }
                            else {
                                uiintro.add('<div><div class="skill">【' + translation + '】</div><div>' + get.skillInfoTranslation(skills[i], node) + '</div></div>');
                            }
                            if (lib.translate[skills[i] + '_append']) {
                                uiintro._place_text = uiintro.add('<div class="text">' + lib.translate[skills[i] + '_append'] + '</div>')
                            }
                        }
                    }

                    if (lib.config.right_range && _status.gameStarted) {
                        uiintro.add(ui.create.div('.placeholder'));
                        var table, tr, td;
                        table = document.createElement('table');
                        tr = document.createElement('tr');
                        table.appendChild(tr);
                        td = document.createElement('td');
                        td.innerHTML = '距离';
                        tr.appendChild(td);
                        td = document.createElement('td');
                        td.innerHTML = '手牌';
                        tr.appendChild(td);
                        td = document.createElement('td');
                        td.innerHTML = '行动';
                        tr.appendChild(td);
                        td = document.createElement('td');
                        td.innerHTML = '伤害';
                        tr.appendChild(td);

                        tr = document.createElement('tr');
                        table.appendChild(tr);
                        td = document.createElement('td');
                        if (node == game.me || !game.me || !game.me.isIn()) {
                            td.innerHTML = '-';
                        }
                        else {
                            var dist1 = get.numStr(Math.max(1, game.me.distanceTo(node)));
                            var dist2 = get.numStr(Math.max(1, node.distanceTo(game.me)));
                            if (dist1 == dist2) {
                                td.innerHTML = dist1;
                            }
                            else {
                                td.innerHTML = dist1 + '/' + dist2;
                            }
                        }
                        tr.appendChild(td);
                        td = document.createElement('td');
                        // Card Limited
                        td.innerHTML = `${node.countCards("h")}/${node.getHandcardLimit()}`;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        td.innerHTML = node.phaseNumber;
                        tr.appendChild(td);
                        td = document.createElement('td');

                        (function () {
                            num = 0;
                            for (var j = 0; j < node.stat.length; j++) {
                                if (typeof node.stat[j].damage == 'number') num += node.stat[j].damage;
                            }
                            td.innerHTML = num;
                        }());
                        tr.appendChild(td);
                        table.style.width = 'calc(100% - 20px)';
                        table.style.marginLeft = '10px';

                        uiintro.content.appendChild(table);
                        if (!lib.config.show_favourite) {
                            table.style.paddingBottom = '5px'
                        }
                    }
                    if (!simple || get.is.phoneLayout()) {
                        var es = node.getCards('e');
                        for (var i = 0; i < es.length; i++) {
                            uiintro.add('<div><div class="skill">' + es[i].outerHTML + '</div><div>' + lib.translate[es[i].name + '_info'] + '</div></div>');
                            uiintro.content.lastChild.querySelector('.skill>.card').style.transform = '';
                        }
                        var js = node.getCards('j');
                        for (var i = 0; i < js.length; i++) {
                            if (js[i].viewAs && js[i].viewAs != js[i].name) {
                                uiintro.add('<div><div class="skill">' + js[i].outerHTML + '</div><div>' + lib.translate[js[i].viewAs] + '：' + lib.translate[js[i].viewAs + '_info'] + '</div></div>');
                            }
                            else {
                                uiintro.add('<div><div class="skill">' + js[i].outerHTML + '</div><div>' + lib.translate[js[i].name + '_info'] + '</div></div>');
                            }
                            uiintro.content.lastChild.querySelector('.skill>.card').style.transform = '';
                        }
                        if (get.is.phoneLayout()) {
                            var markCoutainer = ui.create.div('.mark-container.marks');
                            for (var i in node.marks) {
                                var nodemark = node.marks[i].cloneNode(true);
                                nodemark.classList.add('pointerdiv');
                                nodemark.link = node.marks[i];
                                nodemark.style.transform = '';
                                markCoutainer.appendChild(nodemark);
                                nodemark.listen(function () {
                                    uiintro.noresume = true;
                                    var rect = this.link.getBoundingClientRect();
                                    ui.click.intro.call(this.link, {
                                        clientX: rect.left + rect.width,
                                        clientY: rect.top + rect.height / 2,
                                    });
                                    if (lib.config.touchscreen) {
                                        uiintro._close();
                                    }
                                });
                            }
                            if (markCoutainer.childElementCount) {
                                uiintro.addText('标记');
                                uiintro.add(markCoutainer);
                            }
                        }
                    }
                    if (!game.observe && _status.gameStarted && game.me && node != game.me) {
                        ui.throwEmotion = [];
                        uiintro.addText('发送交互表情');
                        var click = function () {
                            if (_status.dragged) return;
                            if (_status.justdragged) return;
                            if (_status.throwEmotionWait) return;
                            var emotion = this.link;
                            if (game.online) {
                                game.send('throwEmotion', node, emotion);
                            }
                            else game.me.throwEmotion(node, emotion);
                            uiintro._close();
                            _status.throwEmotionWait = true;
                            setTimeout(function () {
                                _status.throwEmotionWait = false;
                                if (ui.throwEmotion) {
                                    for (var i of ui.throwEmotion) i.classList.remove('exclude');
                                }
                            }, (emotion == 'flower' || emotion == 'egg') ? 5000 : 10000)
                        };
                        var td;
                        var table = document.createElement('div');
                        table.classList.add('add-setting');
                        table.style.margin = '0';
                        table.style.width = '100%';
                        table.style.position = 'relative';
                        var listi = ['flower', 'egg'];
                        for (var i = 0; i < listi.length; i++) {
                            td = ui.create.div('.menubutton.reduce_radius.pointerdiv.tdnode');
                            ui.throwEmotion.add(td);
                            if (_status.throwEmotionWait) td.classList.add('exclude');
                            td.link = listi[i];
                            table.appendChild(td);
                            td.innerHTML = '<span>' + get.translation(listi[i]) + '</span>';
                            td.addEventListener(lib.config.touchscreen ? 'touchend' : 'click', click);
                        }
                        uiintro.content.appendChild(table);
                        table = document.createElement('div');
                        table.classList.add('add-setting');
                        table.style.margin = '0';
                        table.style.width = '100%';
                        table.style.position = 'relative';
                        var listi = ['wine', 'shoe'];
                        if (game.me.storage.zhuSkill_shanli) listi = ['yuxisx', 'jiasuo'];
                        for (var i = 0; i < listi.length; i++) {
                            td = ui.create.div('.menubutton.reduce_radius.pointerdiv.tdnode');
                            ui.throwEmotion.add(td);
                            if (_status.throwEmotionWait) td.classList.add('exclude');
                            td.link = listi[i];
                            table.appendChild(td);
                            td.innerHTML = '<span>' + get.translation(listi[i]) + '</span>';
                            td.addEventListener(lib.config.touchscreen ? 'touchend' : 'click', click);
                        }
                        uiintro.content.appendChild(table);
                    }
                    var modepack = lib.characterPack['mode_' + get.mode()];
                    if (lib.config.show_favourite && lib.character[node.name] && game.players.contains(node) &&
                        (!modepack || !modepack[node.name]) && (!simple || get.is.phoneLayout())) {
                        var addFavourite = ui.create.div('.text.center.pointerdiv');
                        addFavourite.link = node.link;
                        if (lib.config.favouriteCharacter.contains(node.name)) {
                            addFavourite.innerHTML = '移除收藏';
                        }
                        else {
                            addFavourite.innerHTML = '添加收藏';
                        }
                        addFavourite.listen(ui.click.favouriteCharacter)
                        uiintro.add(addFavourite);
                    }
                    if (!simple || get.is.phoneLayout()) {
                        if ((lib.config.change_skin || lib.skin) && !node.isUnseen()) {
                            var num = 1;
                            var introadded = false;
                            var createButtons = function (num, avatar2) {
                                if (!introadded) {
                                    introadded = true;
                                    uiintro.add('<div class="text center">更改皮肤</div>');
                                }
                                var buttons = ui.create.div('.buttons.smallzoom.scrollbuttons');
                                lib.setMousewheel(buttons);
                                var nameskin = (avatar2 ? node.name2 : node.name1);
                                var nameskin2 = nameskin;
                                var gzbool = false;
                                if (nameskin.indexOf('gz_shibing') == 0) {
                                    nameskin = nameskin.slice(3, 11);
                                }
                                else if (nameskin.indexOf('gz_') == 0) {
                                    nameskin = nameskin.slice(3);
                                    gzbool = true;
                                }
                                for (var i = 0; i <= num; i++) {
                                    var button = ui.create.div('.button.character.pointerdiv', buttons, function () {
                                        if (this._link) {
                                            if (avatar2) {
                                                lib.config.skin[nameskin] = this._link;
                                                node.node.avatar2.style.backgroundImage = this.style.backgroundImage;
                                            }
                                            else {
                                                lib.config.skin[nameskin] = this._link;
                                                node.node.avatar.style.backgroundImage = this.style.backgroundImage;
                                            }
                                        }
                                        else {
                                            delete lib.config.skin[nameskin];
                                            if (avatar2) {
                                                if (gzbool && lib.character[nameskin2][4].contains('gzskin') && lib.config.mode_config.guozhan.guozhanSkin) node.node.avatar2.setBackground(nameskin2, 'character');
                                                else node.node.avatar2.setBackground(nameskin, 'character');
                                            }
                                            else {
                                                if (gzbool && lib.character[nameskin2][4].contains('gzskin') && lib.config.mode_config.guozhan.guozhanSkin) node.node.avatar.setBackground(nameskin2, 'character');
                                                else node.node.avatar.setBackground(nameskin, 'character');
                                            }
                                        }
                                        game.saveConfig('skin', lib.config.skin);
                                    });
                                    button._link = i;
                                    if (i) {
                                        button.setBackgroundImage('image/skin/' + nameskin + '/' + i + '.jpg');
                                    }
                                    else {
                                        if (gzbool && lib.character[nameskin2][4].contains('gzskin') && lib.config.mode_config.guozhan.guozhanSkin) button.setBackground(nameskin2, 'character', 'noskin');
                                        else button.setBackground(nameskin, 'character', 'noskin');
                                    }
                                }
                                uiintro.add(buttons);
                            };
                            var loadImage = function (avatar2) {
                                var img = new Image();
                                img.onload = function () {
                                    num++;
                                    loadImage(avatar2);
                                }
                                img.onerror = function () {
                                    num--;
                                    if (num) {
                                        createButtons(num, avatar2);
                                    }
                                    if (!avatar2) {
                                        if (!node.classList.contains('unseen2') && node.name2) {
                                            num = 1;
                                            loadImage(true);
                                        }
                                    }
                                }
                                var nameskin = (avatar2 ? node.name2 : node.name1);
                                var nameskin2 = nameskin;
                                var gzbool = false;
                                if (nameskin.indexOf('gz_shibing') == 0) {
                                    nameskin = nameskin.slice(3, 11);
                                }
                                else if (nameskin.indexOf('gz_') == 0) {
                                    nameskin = nameskin.slice(3);
                                    gzbool = true;
                                }
                                img.src = lib.assetURL + 'image/skin/' + nameskin + '/' + num + '.jpg';
                            }
                            if (lib.config.change_skin) {
                                if (!node.isUnseen(0)) {
                                    loadImage();
                                }
                                else if (node.name2) {
                                    loadImage(true);
                                }
                            }
                            else {
                                setTimeout(function () {
                                    var nameskin1 = node.name1;
                                    var nameskin2 = node.name2;
                                    if (nameskin1 && nameskin1.indexOf('gz_') == 0) {
                                        nameskin1 = nameskin1.slice(3);
                                    }
                                    if (nameskin2 && nameskin2.indexOf('gz_') == 0) {
                                        nameskin2 = nameskin2.slice(3);
                                    }
                                    if (!node.isUnseen(0) && lib.skin[nameskin1]) {
                                        createButtons(lib.skin[nameskin1]);
                                    }
                                    if (!node.isUnseen(1) && lib.skin[nameskin2]) {
                                        createButtons(lib.skin[nameskin2], true);
                                    }
                                });
                            }
                        }
                    }

                    uiintro.add(ui.create.div('.placeholder.slim'));
                }
                else if (node.classList.contains('mark') && node.info &&
                    node.parentNode && node.parentNode.parentNode && node.parentNode.parentNode.classList.contains('player')) {
                    var info = node.info;
                    var player = node.parentNode.parentNode;
                    if (info.name) {
                        if (typeof info.name == 'function') {
                            var named = info.name(player.storage[node.skill], player);
                            if (named) {
                                uiintro.add(named);
                            }
                        }
                        else {
                            uiintro.add(info.name);
                        }
                    }
                    else if (info.name !== false) {
                        uiintro.add(get.translation(node.skill));
                    }
                    if (typeof info.id == 'string' && info.id.indexOf('subplayer') == 0 &&
                        player.isUnderControl(true) && player.storage[info.id] && !_status.video) {
                        var storage = player.storage[info.id];
                        uiintro.addText('当前体力：' + storage.hp + '/' + storage.maxHp);
                        if (storage.hs.length) {
                            uiintro.addText('手牌区');
                            uiintro.addSmall(storage.hs);
                        }
                        if (storage.es.length) {
                            uiintro.addText('装备区');
                            uiintro.addSmall(storage.es);
                        }
                    }
                    if (typeof info.mark == 'function') {
                        var stint = info.mark(uiintro, player.storage[node.skill], player);
                        if (stint) {
                            var placetext = uiintro.add('<div class="text" style="display:inline">' + stint + '</div>');
                            if (stint.indexOf('<div class="skill"') != 0) {
                                uiintro._place_text = placetext;
                            }
                            // if(stint.length<=100){
                            // 	uiintro.add('<div class="text center">'+stint+'</div>');
                            // }
                            // else{
                            // 	uiintro.add('<div class="text">'+stint+'</div>');
                            // }
                        }
                    }
                    else {
                        var stint = get.storageintro(info.content, player.storage[node.skill], player, uiintro, node.skill);
                        if (stint) {
                            if (stint[0] == '@') {
                                uiintro.add('<div class="caption">' + stint.slice(1) + '</div>');
                            }
                            else {
                                var placetext = uiintro.add('<div class="text" style="display:inline">' + stint + '</div>');
                                if (stint.indexOf('<div class="skill"') != 0) {
                                    uiintro._place_text = placetext;
                                }
                            }
                            // else if(stint.length<=100){
                            // 	uiintro.add('<div class="text center">'+stint+'</div>');
                            // }
                            // else{
                            // 	uiintro.add('<div class="text">'+stint+'</div>');
                            // }
                        }
                    }
                    uiintro.add(ui.create.div('.placeholder.slim'));
                }
                else if (node.classList.contains('card')) {
                    //卡牌长按介绍
                    if (ui.arena.classList.contains('observe') && node.parentNode.classList.contains('handcards')) {
                        return;
                    }
                    var name = node.name;
                    if (node.parentNode.cardMod) {
                        var moded = false;
                        for (var i in node.parentNode.cardMod) {
                            var item = node.parentNode.cardMod[i](node);
                            if (Array.isArray(item)) {
                                moded = true;
                                uiintro.add(item[0]);
                                uiintro._place_text = uiintro.add('<div class="text" style="display:inline">' + item[1] + '</div>');
                            }
                        }
                        if (moded) return uiintro;
                    }
                    if (node.link && node.link.name && lib.card[node.link.name]) {
                        name = node.link.name;
                    }
                    if (get.position(node) == 'j' && node.viewAs && node.viewAs != name) {
                        uiintro.add(get.translation(node.viewAs));
                        uiintro.add('<div class="text center">（' + get.translation(get.translation(node)) + '）</div>');
                        // uiintro.add(get.translation(node.viewAs)+'<br><div class="text center" style="padding-top:5px;">（'+get.translation(node)+'）</div>');
                        uiintro.nosub = true;
                        name = node.viewAs;
                    }
                    else {
                        uiintro.add(get.translation(node));
                    }
                    if (node._banning) {
                        var clickBanned = function () {
                            var banned = lib.config[this.bannedname] || [];
                            if (banned.contains(name)) {
                                banned.remove(name);
                            }
                            else {
                                banned.push(name);
                            }
                            game.saveConfig(this.bannedname, banned);
                            this.classList.toggle('on');
                            if (node.updateBanned) {
                                node.updateBanned();
                            }
                        };
                        var modeorder = lib.config.modeorder || [];
                        for (var i in lib.mode) {
                            modeorder.add(i);
                        }
                        var list = [];
                        uiintro.contentContainer.listen(function (e) {
                            ui.click.touchpop();
                            e.stopPropagation();
                        });
                        for (var i = 0; i < modeorder.length; i++) {
                            if (node._banning == 'online') {
                                if (!lib.mode[modeorder[i]].connect) continue;
                            }
                            else if (modeorder[i] == 'connect' || modeorder[i] == 'brawl') {
                                continue;
                            }
                            if (lib.config.all.mode.contains(modeorder[i])) {
                                list.push(modeorder[i]);
                            }
                        }
                        if (lib.card[name] && lib.card[name].type == 'trick') list.push('zhinang_tricks');
                        var page = ui.create.div('.menu-buttons.configpopped', uiintro.content);
                        var banall = false;
                        for (var i = 0; i < list.length; i++) {
                            var cfg = ui.create.div('.config', list[i] == 'zhinang_tricks' ? '设为智囊' : (lib.translate[list[i]] + '模式'), page);
                            cfg.classList.add('toggle');
                            if (list[i] == 'zhinang_tricks') {
                                cfg.bannedname = ((node._banning == 'offline') ? '' : 'connect_') + 'zhinang_tricks';
                            }
                            else if (node._banning == 'offline') {
                                cfg.bannedname = list[i] + '_bannedcards';
                            }
                            else {
                                cfg.bannedname = 'connect_' + list[i] + '_bannedcards';
                            }
                            cfg.listen(clickBanned);
                            ui.create.div(ui.create.div(cfg));
                            var banned = lib.config[cfg.bannedname] || [];
                            if (banned.contains(name) == (list[i] == 'zhinang_tricks')) {
                                cfg.classList.add('on');
                                banall = true;
                            }
                        }
                        ui.create.div('.menubutton.pointerdiv', banall ? '全部禁用' : '全部启用', uiintro.content, function () {
                            if (this.innerHTML == '全部禁用') {
                                for (var i = 0; i < page.childElementCount; i++) {
                                    if (page.childNodes[i].bannedname.indexOf('zhinang_tricks') == -1 && page.childNodes[i].bannedname && page.childNodes[i].classList.contains('on')) {
                                        clickBanned.call(page.childNodes[i]);
                                    }
                                }
                                this.innerHTML = '全部启用';
                            }
                            else {
                                for (var i = 0; i < page.childElementCount; i++) {
                                    if (page.childNodes[i].bannedname.indexOf('zhinang_tricks') == -1 && page.childNodes[i].bannedname && !page.childNodes[i].classList.contains('on')) {
                                        clickBanned.call(page.childNodes[i]);
                                    }
                                }
                                this.innerHTML = '全部禁用';
                            }
                        }).style.marginTop = '-10px';
                        ui.create.div('.placeholder.slim', uiintro.content);
                    }
                    else {
                        if (lib.translate[name + '_info']) {
                            if (!uiintro.nosub) {
                                if (get.subtype(node) == 'equip1') {
                                    var added = false;
                                    if (lib.card[node.name] && lib.card[node.name].distance) {
                                        var dist = lib.card[node.name].distance;
                                        if (dist.attackFrom) {
                                            added = true;
                                            uiintro.add('<div class="text center">攻击范围：' + (-dist.attackFrom + 1) + '</div>');
                                        }
                                    }
                                    if (!added) {
                                        uiintro.add('<div class="text center">攻击范围：1</div>');
                                    }
                                }
                                else if (get.subtype(node)) {
                                    uiintro.add('<div class="text center">' + get.translation(get.subtype(node)) + '</div>');
                                }
                                else if (lib.card[name] && lib.card[name].addinfomenu) {
                                    uiintro.add('<div class="text center">' + lib.card[name].addinfomenu + '</div>');
                                }
                                else if (lib.card[name] && lib.card[name].derivation) {
                                    if (typeof lib.card[name].derivation == 'string') {
                                        uiintro.add('<div class="text center">来源：' + get.translation(lib.card[name].derivation) + '</div>');
                                    }
                                    else if (lib.card[name].derivationpack) {
                                        uiintro.add('<div class="text center">来源：' + get.translation(lib.card[name].derivationpack + '_card_config') + '包</div>');
                                    }
                                }
                                else {
                                    if (lib.card[name].unique) {
                                        uiintro.add('<div class="text center">特殊' + get.translation(lib.card[name].type) + '牌</div>');
                                    }
                                    else {
                                        if (lib.card[name].type && lib.translate[lib.card[name].type]) uiintro.add('<div class="text center">' + get.translation(lib.card[name].type) + '牌</div>');
                                    }
                                }
                                if (lib.card[name].unique && lib.card[name].type == 'equip') {
                                    if (lib.cardPile.guozhan && lib.cardPack.guozhan.contains(name)) {
                                        uiintro.add('<div class="text center">专属装备</div>').style.marginTop = '-5px';
                                    }
                                    else {
                                        uiintro.add('<div class="text center">特殊装备</div>').style.marginTop = '-5px';
                                    }
                                }
                            }
                            if (lib.card[name].cardPrompt) {
                                var str = lib.card[name].cardPrompt(node.link || node), placetext = uiintro.add('<div class="text" style="display:inline">' + str + '</div>');
                                if (str.indexOf('<div class="skill"') != 0) {
                                    uiintro._place_text = placetext;
                                }
                            }
                            else if (lib.translate[name + '_info']) {
                                var placetext = uiintro.add('<div class="text" style="display:inline">' + lib.translate[name + '_info'] + '</div>');
                                if (lib.translate[name + '_info'].indexOf('<div class="skill"') != 0) {
                                    uiintro._place_text = placetext;
                                }
                            }
                            if (lib.card[name].yingbian_prompt && get.is.yingbian(node.link || node)) {
                                if (typeof lib.card[name].yingbian_prompt == 'function') uiintro.add('<div class="text" style="font-family: yuanli">应变：' + lib.card[name].yingbian_prompt(node.link || node) + '</div>');
                                else uiintro.add('<div class="text" style="font-family: yuanli">应变：' + lib.card[name].yingbian_prompt + '</div>');
                            }
                            if (lib.translate[name + '_append']) {
                                uiintro.add('<div class="text" style="display:inline">' + lib.translate[name + '_append'] + '</div>');
                            }
                        }
                        uiintro.add(ui.create.div('.placeholder.slim'));
                    }
                }
                else if (node.classList.contains('character')) {
                    var character = node.link;
                    if (lib.character[node.link] && lib.character[node.link][1]) {
                        var group = get.is.double(node.link, true);
                        if (group) {
                            var str = get.translation(character) + '&nbsp;&nbsp;';
                            for (var i = 0; i < group.length; i++) {
                                str += get.translation(group[i]);
                                if (i < group.length - 1) str += '/';
                            }
                            uiintro.add(str);
                        }
                        else uiintro.add(get.translation(character) + '&nbsp;&nbsp;' + lib.translate[lib.character[node.link][1]]);
                    }
                    else {
                        uiintro.add(get.translation(character));
                    }

                    if (lib.characterTitle[node.link]) {
                        uiintro.addText(get.colorspan(lib.characterTitle[node.link]));
                    }

                    if (node._banning) {
                        var clickBanned = function () {
                            var banned = lib.config[this.bannedname] || [];
                            if (banned.contains(character)) {
                                banned.remove(character);
                            }
                            else {
                                banned.push(character);
                            }
                            game.saveConfig(this.bannedname, banned);
                            this.classList.toggle('on');
                            if (node.updateBanned) {
                                node.updateBanned();
                            }
                        };
                        var modeorder = lib.config.modeorder || [];
                        for (var i in lib.mode) {
                            modeorder.add(i);
                        }
                        var list = [];
                        uiintro.contentContainer.listen(function (e) {
                            ui.click.touchpop();
                            e.stopPropagation();
                        });
                        for (var i = 0; i < modeorder.length; i++) {
                            if (node._banning == 'online') {
                                if (!lib.mode[modeorder[i]].connect) continue;
                                if (!lib.config['connect_' + modeorder[i] + '_banned']) {
                                    lib.config['connect_' + modeorder[i] + '_banned'] = [];
                                }
                            }
                            else if (modeorder[i] == 'connect' || modeorder[i] == 'brawl') {
                                continue;
                            }
                            if (lib.config.all.mode.contains(modeorder[i])) {
                                list.push(modeorder[i]);
                            }
                        }
                        var page = ui.create.div('.menu-buttons.configpopped', uiintro.content);
                        var banall = false;
                        for (var i = 0; i < list.length; i++) {
                            var cfg = ui.create.div('.config', lib.translate[list[i]] + '模式', page);
                            cfg.classList.add('toggle');
                            if (node._banning == 'offline') {
                                cfg.bannedname = list[i] + '_banned';
                            }
                            else {
                                cfg.bannedname = 'connect_' + list[i] + '_banned';
                            }
                            cfg.listen(clickBanned);
                            ui.create.div(ui.create.div(cfg));
                            var banned = lib.config[cfg.bannedname] || [];
                            if (!banned.contains(character)) {
                                cfg.classList.add('on');
                                banall = true;
                            }
                        }
                        if (node._banning == 'offline') {
                            var cfg = ui.create.div('.config', '随机选将可用', page);
                            cfg.classList.add('toggle');
                            cfg.listen(function () {
                                this.classList.toggle('on');
                                if (this.classList.contains('on')) {
                                    lib.config.forbidai_user.remove(character);
                                }
                                else {
                                    lib.config.forbidai_user.add(character);
                                }
                                game.saveConfig('forbidai_user', lib.config.forbidai_user);
                            });
                            ui.create.div(ui.create.div(cfg));
                            if (!lib.config.forbidai_user.contains(character)) {
                                cfg.classList.add('on');
                            }
                        }
                        ui.create.div('.menubutton.pointerdiv', banall ? '全部禁用' : '全部启用', uiintro.content, function () {
                            if (this.innerHTML == '全部禁用') {
                                for (var i = 0; i < page.childElementCount; i++) {
                                    if (page.childNodes[i].bannedname && page.childNodes[i].classList.contains('on')) {
                                        clickBanned.call(page.childNodes[i]);
                                    }
                                }
                                this.innerHTML = '全部启用';
                            }
                            else {
                                for (var i = 0; i < page.childElementCount; i++) {
                                    if (page.childNodes[i].bannedname && !page.childNodes[i].classList.contains('on')) {
                                        clickBanned.call(page.childNodes[i]);
                                    }
                                }
                                this.innerHTML = '全部禁用';
                            }
                        }).style.marginTop = '-10px';
                        ui.create.div('.placeholder.slim', uiintro.content);
                    }
                    else {
                        var infoitem = lib.character[character];
                        if (!infoitem) {
                            for (var itemx in lib.characterPack) {
                                if (lib.characterPack[itemx][character]) {
                                    infoitem = lib.characterPack[itemx][character]; break;
                                }
                            }
                        }
                        var skills = infoitem[3];
                        for (i = 0; i < skills.length; i++) {
                            if (lib.translate[skills[i] + '_info']) {
                                translation = lib.translate[skills[i] + '_ab'] || get.translation(skills[i]).slice(0, 2);
                                if (lib.skill[skills[i]] && lib.skill[skills[i]].nobracket) {
                                    uiintro.add('<div><div class="skilln">' + get.translation(skills[i]) + '</div><div>' + get.skillInfoTranslation(skills[i]) + '</div></div>');
                                }
                                else {
                                    uiintro.add('<div><div class="skill">【' + translation + '】</div><div>' + get.skillInfoTranslation(skills[i]) + '</div></div>');
                                }
                                if (lib.translate[skills[i] + '_append']) {
                                    uiintro._place_text = uiintro.add('<div class="text">' + lib.translate[skills[i] + '_append'] + '</div>')
                                }
                            }
                        }
                        var modepack = lib.characterPack['mode_' + get.mode()];
                        if (lib.config.show_favourite &&
                            lib.character[node.link] && (!modepack || !modepack[node.link]) && (!simple || get.is.phoneLayout())) {
                            var addFavourite = ui.create.div('.text.center.pointerdiv');
                            addFavourite.link = node.link;
                            addFavourite.style.marginBottom = '15px';
                            if (lib.config.favouriteCharacter.contains(node.link)) {
                                addFavourite.innerHTML = '移除收藏';
                            }
                            else {
                                addFavourite.innerHTML = '添加收藏';
                            }
                            addFavourite.listen(ui.click.favouriteCharacter)
                            uiintro.add(addFavourite);
                        }
                        else {
                            uiintro.add(ui.create.div('.placeholder.slim'));
                        }
                        var addskin = false;
                        if (node.parentNode.classList.contains('menu-buttons')) {
                            addskin = !lib.config.show_charactercard;
                        }
                        else {
                            addskin = lib.config.change_skin || lib.skin;
                        }
                        if (addskin && (!simple || get.is.phoneLayout())) {
                            var num = 1;
                            var introadded = false;
                            var nameskin = node.link;
                            var nameskin2 = nameskin;
                            var gzbool = false;
                            if (nameskin.indexOf('gz_shibing') == 0) {
                                nameskin = nameskin.slice(3, 11);
                            }
                            else if (nameskin.indexOf('gz_') == 0) {
                                nameskin = nameskin.slice(3);
                                gzbool = true;
                            }
                            var createButtons = function (num) {
                                if (!num) return;
                                if (!introadded) {
                                    introadded = true;
                                    uiintro.add('<div class="text center">更改皮肤</div>');
                                }
                                var buttons = ui.create.div('.buttons.smallzoom.scrollbuttons');
                                lib.setMousewheel(buttons);
                                for (var i = 0; i <= num; i++) {
                                    var button = ui.create.div('.button.character.pointerdiv', buttons, function () {
                                        if (this._link) {
                                            lib.config.skin[nameskin] = this._link;
                                            node.style.backgroundImage = this.style.backgroundImage;
                                            game.saveConfig('skin', lib.config.skin);
                                        }
                                        else {
                                            delete lib.config.skin[nameskin];
                                            if (gzbool && lib.character[nameskin2][4].contains('gzskin') && lib.config.mode_config.guozhan.guozhanSkin) node.setBackground(nameskin2, 'character');
                                            else node.setBackground(nameskin, 'character');
                                            game.saveConfig('skin', lib.config.skin);
                                        }
                                    });
                                    button._link = i;
                                    if (i) {
                                        button.setBackgroundImage('image/skin/' + nameskin + '/' + i + '.jpg');
                                    }
                                    else {
                                        if (gzbool && lib.character[nameskin2][4].contains('gzskin') && lib.config.mode_config.guozhan.guozhanSkin) button.setBackground(nameskin2, 'character', 'noskin');
                                        else button.setBackground(nameskin, 'character', 'noskin');
                                    }
                                }
                                uiintro.add(buttons);
                            };
                            var loadImage = function () {
                                var img = new Image();
                                img.onload = function () {
                                    num++;
                                    loadImage();
                                }
                                img.onerror = function () {
                                    num--;
                                    createButtons(num);
                                }
                                img.src = lib.assetURL + 'image/skin/' + nameskin + '/' + num + '.jpg';
                            }
                            if (lib.config.change_skin) {
                                loadImage();
                            }
                            else {
                                setTimeout(function () {
                                    createButtons(lib.skin[nameskin]);
                                });
                            }
                        }
                    }
                }
                else if (node.classList.contains('equips') && ui.arena.classList.contains('selecting')) {
                    (function () {
                        uiintro.add('选择装备');
                        uiintro.addSmall(Array.from(node.childNodes), true);
                        uiintro.clickintro = true;
                        ui.control.hide();
                        uiintro._onclose = function () {
                            ui.control.show();
                        }
                        var confirmbutton;
                        for (var i = 0; i < uiintro.buttons.length; i++) {
                            var button = uiintro.buttons[i];
                            button.classList.add('pointerdiv');
                            if (button.link.classList.contains('selected')) {
                                button.classList.add('selected');
                            }
                            button.listen(function (e) {
                                ui.click.card.call(this.link, 'popequip');
                                ui.click.window.call(ui.window, e);
                                if (this.link.classList.contains('selected')) {
                                    this.classList.add('selected');
                                }
                                else {
                                    this.classList.remove('selected');
                                }
                                if (ui.confirm && ui.confirm.str && ui.confirm.str.indexOf('o') != -1) {
                                    confirmbutton.classList.remove('disabled');
                                }
                                else {
                                    confirmbutton.classList.add('disabled');
                                }
                            });
                        }
                        var buttoncontainer = uiintro.add(ui.create.div());
                        buttoncontainer.style.display = 'block';
                        confirmbutton = ui.create.div('.menubutton.large.pointerdiv', '确定', function () {
                            if (ui.confirm && ui.confirm.str && ui.confirm.str.indexOf('o') != -1) {
                                uiintro._clickintro();
                                ui.click.ok(ui.confirm.firstChild);
                            }
                        }, buttoncontainer);
                        confirmbutton.style.position = 'relative';
                        setTimeout(function () {
                            if (ui.confirm && ui.confirm.str && ui.confirm.str.indexOf('o') != -1) {
                                confirmbutton.classList.remove('disabled');
                            }
                            else {
                                confirmbutton.classList.add('disabled');
                            }
                        }, 300);
                    }());
                }
                else if (node.classList.contains('identity') && node.dataset.career) {
                    var career = node.dataset.career;
                    uiintro.add(get.translation(career));
                    uiintro.add('<div class="text center" style="padding-bottom:5px">' + lib.translate['_' + career + '_skill_info'] + '</div>');
                }
                else if (node.classList.contains('skillbar')) {
                    if (node == ui.friendBar) {
                        uiintro.add('友方怒气值');
                        uiintro.add('<div class="text center" style="padding-bottom:5px">' + _status.friendRage + '/100</div>');
                    }
                    else if (node == ui.enemyBar) {
                        uiintro.add('敌方怒气值');
                        uiintro.add('<div class="text center" style="padding-bottom:5px">' + _status.enemyRage + '/100</div>');
                    }
                }
                else if (node.parentNode == ui.historybar) {
                    if (node.dead) {
                        if (!node.source || node.source == node.player) {
                            uiintro.add('<div class="text center">' + get.translation(node.player) + '阵亡</div>');
                            uiintro.addSmall([node.player]);
                        }
                        else {
                            uiintro.add('<div class="text center">' + get.translation(node.player) + '被' + get.translation(node.source) + '杀害</div>');
                            uiintro.addSmall([node.source]);
                        }
                    }
                    if (node.skill) {
                        uiintro.add('<div class="text center">' + get.translation(node.skill, 'skill') + '</div>');
                        uiintro._place_text = uiintro.add('<div class="text" style="display:inline">' + get.translation(node.skill, 'info') + '</div>');
                    }
                    if (node.targets && get.itemtype(node.targets) == 'players') {
                        uiintro.add('<div class="text center">目标</div>');
                        uiintro.addSmall(node.targets);
                    }
                    if (node.players && node.players.length > 1) {
                        uiintro.add('<div class="text center">使用者</div>');
                        uiintro.addSmall(node.players);
                    }
                    if (node.cards && node.cards.length) {
                        uiintro.add('<div class="text center">卡牌</div>');
                        uiintro.addSmall(node.cards);
                    }
                    for (var i = 0; i < node.added.length; i++) {
                        uiintro.add(node.added[i]);
                    }
                    if (node.added.length) {
                        uiintro.add(ui.create.div('.placeholder.slim'));
                    }
                    if (uiintro.content.firstChild) {
                        uiintro.content.firstChild.style.paddingTop = '3px';
                    }
                }
                if (lib.config.touchscreen) {
                    lib.setScroll(uiintro.contentContainer);
                }
                return uiintro;
            };
        };
        // Alias Methods
        const alias = () => {
            lib.element.player["alias_init"] = function () {
                //alert("alias_init")
                if (!this["alias_record"]) this["alias_record"] = {};
            }
            lib.element.player["alias_create"] = function (name) {
                //alert("alias_create")
                // 防呆初始化
                this["alias_init"]();
                // 创建对应函数名称的列表
                if (!this["alias_record"][name]) this["alias_record"][name] = [];
                // 如果没有原始函数，则创建
                if (!this["alias_function_" + name + "_origin"] || !this["alias_record"][name].contains("origin")) {
                    this["alias_record"][name][0] = "origin";
                    this["alias_function_" + name + "_origin"] = this[name];
                    lib.element.content["alias_function_" + name + "_origin"] = lib.element.content[name]
                }
                // 替换函数
                if (!this["alias_function_" + name + "_origin_bool"]) {
                    this["alias_function_" + name + "_origin_bool"] = true;
                    this[name] = function () {
                        //alert(Array.from(arguments));
                        return this[
                            "alias_function_" + name + "_" + this["alias_record"][name].last()
                        ](...arguments);
                    }
                }
            }
            lib.element.player["alias_add"] = function (name, tag, skill) {
                //alert("alias_add")
                this["alias_create"](name);
                if (!this["alias_record"][name].contains(tag)) this["alias_record"][name].push(tag);
                if (!this["alias_function_" + name + "_" + tag]) this["alias_function_" + name + "_" + tag] = skill;
            }
            lib.element.player["alias_remove"] = function (name, tag, skill) {
                this["alias_add"](name, tag, skill);
                if (this["alias_record"][name].contains(tag)) this["alias_record"][name].remove(tag);
                if (this["alias_function_" + name + "_" + tag]) delete this["alias_function_" + name + "_" + tag];
            }
            lib.element.player["alias_index"] = function (name, tag) {
                //alert("alias_index")
                this["alias_add"](name, tag, function () { });
                return this["alias_record"][name].indexOf(tag);
            }
            lib.element.player["alias_find"] = function (name, index, args) {
                //alert("alias_find")

                return this[
                    "alias_function_" + name + "_" + this["alias_record"][name][index]
                ](...args);
            }
            lib.element.player["alias_destory"] = (name, index) => {
                //
            }
        };
        // Visualen Card Function
        const visualMethods = () => {
            lib.element.content.choosePlayerCard = function () {
                "step 0"
                if (!event.dialog) event.dialog = ui.create.dialog("hidden");
                else if (!event.isMine) {
                    event.dialog.style.display = "none";
                }
                if (event.prompt) {
                    event.dialog.add(event.prompt);
                }
                else {
                    event.dialog.add("选择" + get.translation(target) + "的一张牌");
                }
                if (event.prompt2) {
                    event.dialog.addText(event.prompt2);
                }
                var directh = !lib.config.unauto_choose;
                for (var i = 0; i < event.position.length; i++) {
                    if (event.position[i] == "h") {
                        if (event.onlyVisualed) {
                            let hs = target.getGainableCards(player, "h").filter(card => card.hasGaintag("visualCard"));
                            if (hs.length) {
                                event.dialog.addText("手牌区");
                                event.dialog.add([hs]);
                                directh = false;
                            }
                        }
                        else if (event.onlyUnVisualed) {
                            let hs = target.getGainableCards(player, "h").filter(card => !card.hasGaintag("visualCard"));
                            if (hs.length) {
                                event.dialog.addText("手牌区");
                                if (event.visible || target.isUnderControl(true) || player.hasSkillTag("viewHandcard", null, target, true)) {
                                    event.dialog.add(hs);
                                    directh = false;
                                }
                                else event.dialog.add([hs, "blank"]);
                            }
                        }
                        else {
                            var hs = target.getGainableCards(player, "h");
                            if (hs.length) {
                                event.dialog.addText("手牌区");
                                if (event.visible || target.isUnderControl(true) || player.hasSkillTag("viewHandcard", null, target, true)) {
                                    event.dialog.add(hs);
                                    directh = false;
                                }
                                else {
                                    let hs2 = hs.filter(card => card.hasGaintag("visualCard"));
                                    if (hs2.length) {
                                        let hs1 = hs.filter(card => !card.hasGaintag("visualCard"));
                                        event.dialog.add(hs2);
                                        if (hs1.length) {
                                            hs1.randomSort();
                                            event.dialog.add([hs1, "blank"]);
                                        }
                                        directh = false;
                                    }
                                    else {
                                        hs.randomSort();
                                        event.dialog.add([hs, "blank"]);
                                    }
                                }
                            }
                        }
                    }
                    else if (event.position[i] == "e") {
                        var es = target.getCards("e");
                        if (es.length) {
                            event.dialog.addText("装备区");
                            event.dialog.add(es);
                            directh = false;
                        }
                    }
                    else if (event.position[i] == "j") {
                        var js = target.getCards("j");
                        if (js.length) {
                            event.dialog.addText("判定区");
                            event.dialog.add(js);
                            directh = false;
                        }
                    }
                }
                if (event.dialog.buttons.length == 0) {
                    event.finish();
                    return;
                }
                var cs = target.getCards(event.position);
                var select = get.select(event.selectButton);
                if (event.forced && select[0] >= cs.length) {
                    event.result = {
                        bool: true,
                        buttons: event.dialog.buttons,
                        links: cs
                    }
                }
                else if (event.forced && directh && !event.isOnline() && select[0] == select[1]) {
                    event.result = {
                        bool: true,
                        buttons: event.dialog.buttons.randomGets(select[0]),
                        links: []
                    }
                    for (var i = 0; i < event.result.buttons.length; i++) {
                        event.result.links[i] = event.result.buttons[i].link;
                    }
                }
                else {
                    if (event.isMine()) {
                        if (event.hsskill && !event.forced && _status.prehidden_skills.contains(event.hsskill)) {
                            ui.click.cancel();
                            return;
                        }
                        event.dialog.open();
                        game.check();
                        game.pause();
                    }
                    else if (event.isOnline()) {
                        event.send();
                    }
                    else {
                        event.result = "ai";
                    }
                }
                "step 1"
                if (event.result == "ai") {
                    game.check();
                    if (ai.basic.chooseButton(event.ai) || forced) ui.click.ok();
                    else ui.click.cancel();
                }
                event.dialog.close();
                if (event.result.links) {
                    event.result.cards = event.result.links.slice(0);
                }
                event.resume();
            };

            lib.element.content.discardPlayerCard = function () {
                "step 0"
                if (event.directresult) {
                    event.result = {
                        buttons: [],
                        cards: event.directresult.slice(0),
                        links: event.directresult.slice(0),
                        targets: [],
                        confirm: "ok",
                        bool: true
                    };
                    event.cards = event.directresult.slice(0);
                    event.goto(2);
                    return;
                }
                if (!event.dialog) event.dialog = ui.create.dialog("hidden");
                else if (!event.isMine) {
                    event.dialog.style.display = "none";
                }
                if (event.prompt == undefined) {
                    var str = "弃置" + get.translation(target);
                    var range = get.select(event.selectButton);
                    if (range[0] == range[1]) str += get.cnNumber(range[0]);
                    else if (range[1] == Infinity) str += "至少" + get.cnNumber(range[0]);
                    else str += get.cnNumber(range[0]) + "至" + get.cnNumber(range[1]);
                    str += "张";
                    if (event.position == "h" || event.position == undefined) str += "手";
                    if (event.position == "e") str += "装备";
                    str += "牌";
                    event.prompt = str;
                }
                if (event.prompt) {
                    event.dialog.add(event.prompt);
                }
                if (event.prompt2) {
                    event.dialog.addText(event.prompt2);
                }
                var directh = (!lib.config.unauto_choose && !event.complexSelect);
                for (var i = 0; i < event.position.length; i++) {
                    if (event.position[i] == "h") {
                        if (event.onlyVisualed) {
                            let hs = hs.filter(card => card.hasGaintag("visualCard"));
                            if (hs.length) {
                                event.dialog.addText("手牌区");
                                event.dialog.add([hs, "blank"]);
                                directh = false;
                            }
                        }
                        else {
                            var hs = target.getGainableCards(player, "h");
                            if (hs.length) {
                                event.dialog.addText("手牌区");
                                if (event.visible || target.isUnderControl(true) || player.hasSkillTag("viewHandcard", null, target, true)) {
                                    event.dialog.add(hs);
                                    directh = false;
                                }
                                else {
                                    let hs2 = hs.filter(card => card.hasGaintag("visualCard"));
                                    if (hs2.length) {
                                        let hs1 = hs.filter(card => !card.hasGaintag("visualCard"));
                                        event.dialog.add(hs2);
                                        if (hs1.length) {
                                            hs1.randomSort();
                                            event.dialog.add([hs1, "blank"]);
                                        }
                                        directh = false;
                                    }
                                    else {
                                        hs.randomSort();
                                        event.dialog.add([hs, "blank"]);
                                    }
                                }
                            }
                        }
                    }
                    else if (event.position[i] == "e") {
                        var es = target.getDiscardableCards(player, "e");
                        if (es.length) {
                            event.dialog.addText("装备区");
                            event.dialog.add(es);
                            directh = false;
                        }
                    }
                    else if (event.position[i] == "j") {
                        var js = target.getDiscardableCards(player, "j");
                        if (js.length) {
                            event.dialog.addText("判定区");
                            event.dialog.add(js);
                            directh = false;
                        }
                    }
                }
                if (event.dialog.buttons.length == 0) {
                    event.finish();
                    return;
                }
                var cs = target.getCards(event.position);
                var select = get.select(event.selectButton);
                if (event.forced && select[0] >= cs.length) {
                    event.result = {
                        bool: true,
                        buttons: event.dialog.buttons,
                        links: cs
                    }
                }
                else if (event.forced && directh && !event.isOnline() && select[0] == select[1]) {
                    event.result = {
                        bool: true,
                        buttons: event.dialog.buttons.randomGets(select[0]),
                        links: []
                    }
                    for (var i = 0; i < event.result.buttons.length; i++) {
                        event.result.links[i] = event.result.buttons[i].link;
                    }
                }
                else {
                    if (event.isMine()) {
                        event.dialog.open();
                        game.check();
                        game.pause();
                    }
                    else if (event.isOnline()) {
                        event.send();
                    }
                    else {
                        event.result = "ai";
                    }
                }
                "step 1"
                if (event.result == "ai") {
                    game.check();
                    if (ai.basic.chooseButton(event.ai) || forced) ui.click.ok();
                    else ui.click.cancel();
                }
                event.dialog.close();
                "step 2"
                event.resume();
                if (event.result.bool && event.result.links && !game.online) {
                    if (event.logSkill) {
                        if (typeof event.logSkill == "string") {
                            player.logSkill(event.logSkill);
                        }
                        else if (Array.isArray(event.logSkill)) {
                            player.logSkill.apply(player, event.logSkill);
                        }
                    }
                    var cards = [];
                    for (var i = 0; i < event.result.links.length; i++) {
                        cards.push(event.result.links[i]);
                    }
                    event.result.cards = event.result.links.slice(0);
                    event.cards = cards;
                    event.trigger("rewriteDiscardResult");
                }
                "step 3"
                if (event.boolline) {
                    player.line(target, "green");
                }
                if (!event.chooseonly) {
                    var next = target.discard(event.cards);
                    if (player != target) next.notBySelf = true;
                    event.done = next;
                    if (event.delay === false) {
                        next.set("delay", false);
                    }
                }
            };

            lib.element.content.gainPlayerCard = function () {
                "step 0"
                if (event.directresult) {
                    event.result = {
                        buttons: [],
                        cards: event.directresult.slice(0),
                        links: event.directresult.slice(0),
                        targets: [],
                        confirm: "ok",
                        bool: true
                    };
                    event.cards = event.directresult.slice(0);
                    event.goto(2);
                    return;
                }
                if (!event.dialog) event.dialog = ui.create.dialog("hidden");
                else if (!event.isMine) {
                    event.dialog.style.display = "none";
                }
                if (event.prompt == undefined) {
                    var str = "获得" + get.translation(target);
                    var range = get.select(event.selectButton);
                    if (range[0] == range[1]) str += get.cnNumber(range[0]);
                    else if (range[1] == Infinity) str += "至少" + get.cnNumber(range[0]);
                    else str += get.cnNumber(range[0]) + "至" + get.cnNumber(range[1]);
                    str += "张";
                    if (event.position == "h" || event.position == undefined) str += "手";
                    if (event.position == "e") str += "装备";
                    str += "牌";
                    event.prompt = str;
                }
                if (event.prompt) {
                    event.dialog.add(event.prompt);
                }
                if (event.prompt2) {
                    event.dialog.addText(event.prompt2);
                }
                var directh = (!lib.config.unauto_choose && !event.complexSelect);
                for (var i = 0; i < event.position.length; i++) {
                    if (event.position[i] == "h") {
                        if (event.onlyVisualed) {
                            let hs = hs.filter(card => card.hasGaintag("visualCard"));
                            if (hs.length) {
                                event.dialog.addText("手牌区");
                                event.dialog.add([hs, "blank"]);
                                directh = false;
                            }
                        }
                        else {
                            var hs = target.getGainableCards(player, "h");
                            if (hs.length) {
                                event.dialog.addText("手牌区");
                                if (event.visible || target.isUnderControl(true) || player.hasSkillTag("viewHandcard", null, target, true)) {
                                    event.dialog.add(hs);
                                    directh = false;
                                }
                                else {
                                    let hs2 = hs.filter(card => card.hasGaintag("visualCard"));
                                    if (hs2.length) {
                                        let hs1 = hs.filter(card => !card.hasGaintag("visualCard"));
                                        event.dialog.add(hs2);
                                        if (hs1.length) {
                                            hs1.randomSort();
                                            event.dialog.add([hs1, "blank"]);
                                        }
                                        directh = false;
                                    }
                                    else {
                                        hs.randomSort();
                                        event.dialog.add([hs, "blank"]);
                                    }
                                }
                            }
                        }
                    }
                    else if (event.position[i] == "e") {
                        var es = target.getGainableCards(player, "e");
                        if (es.length) {
                            event.dialog.addText("装备区");
                            event.dialog.add(es);
                            directh = false;
                        }
                    }
                    else if (event.position[i] == "j") {
                        var js = target.getGainableCards(player, "j");
                        if (js.length) {
                            event.dialog.addText("判定区");
                            event.dialog.add(js);
                            directh = false;
                        }
                    }
                }
                if (event.dialog.buttons.length == 0) {
                    event.dialog.close();
                    event.finish();
                    return;
                }
                var cs = target.getCards(event.position);
                var select = get.select(event.selectButton);
                if (event.forced && select[0] >= cs.length) {
                    event.result = {
                        bool: true,
                        buttons: event.dialog.buttons,
                        links: cs
                    }
                }
                else if (event.forced && directh && !event.isOnline() && select[0] == select[1]) {
                    event.result = {
                        bool: true,
                        buttons: event.dialog.buttons.randomGets(select[0]),
                        links: []
                    }
                    for (var i = 0; i < event.result.buttons.length; i++) {
                        event.result.links[i] = event.result.buttons[i].link;
                    }
                }
                else {
                    if (event.isMine()) {
                        event.dialog.open();
                        game.check();
                        game.pause();
                    }
                    else if (event.isOnline()) {
                        event.send();
                    }
                    else {
                        event.result = "ai";
                    }
                }
                "step 1"
                if (event.result == "ai") {
                    game.check();
                    if (ai.basic.chooseButton(event.ai) || forced) ui.click.ok();
                    else ui.click.cancel();
                }
                event.dialog.close();
                "step 2"
                event.resume();
                if (game.online || !event.result.bool) {
                    event.finish();
                }
                "step 3"
                if (event.logSkill && event.result.bool && !game.online) {
                    if (typeof event.logSkill == "string") {
                        player.logSkill(event.logSkill);
                    }
                    else if (Array.isArray(event.logSkill)) {
                        player.logSkill.apply(player, event.logSkill);
                    }
                }
                var cards = [];
                for (var i = 0; i < event.result.links.length; i++) {
                    cards.push(event.result.links[i]);
                }
                event.result.cards = event.result.links.slice(0);
                event.cards = cards;
                event.trigger("rewriteGainResult");
                "step 4"
                if (event.boolline) {
                    player.line(target, "green");
                }
                if (!event.chooseonly) {
                    if (event.delay !== false) {
                        var next = player.gain(event.cards, target, event.visibleMove ? "give" : "giveAuto", "bySelf");
                        event.done = next;
                    }
                    else {
                        var next = player.gain(event.cards, target, "bySelf");
                        event.done = next;
                        target[event.visibleMove ? "$give" : "$giveAuto"](cards, player);
                        if (event.visibleMove) next.visible = true;
                    }
                }
                else target[event.visibleMove ? "$give" : "$giveAuto"](cards, player);
            };
        };
        // Die Audio
        const dieAudio = () => {
            lib.element.content.die = function () {
                "step 0"
                event.forceDie = true;
                if (_status.roundStart == player) {
                    _status.roundStart = player.next || player.getNext() || game.players[0];
                }
                if (ui.land && ui.land.player == player) {
                    game.addVideo('destroyLand');
                    ui.land.destroy();
                }
                var unseen = false;
                if (player.classList.contains('unseen')) {
                    player.classList.remove('unseen');
                    unseen = true;
                }
                var logvid = game.logv(player, 'die', source);
                event.logvid = logvid;
                if (unseen) {
                    player.classList.add('unseen');
                }
                if (source) {
                    game.log(player, '被', source, '杀害');
                    if (source.stat[source.stat.length - 1].kill == undefined) {
                        source.stat[source.stat.length - 1].kill = 1;
                    }
                    else {
                        source.stat[source.stat.length - 1].kill++;
                    }
                }
                else {
                    game.log(player, '阵亡')
                }

                game.broadcastAll(function (player) {
                    player.classList.add('dead');
                    player.removeLink();
                    player.classList.remove('turnedover');
                    player.classList.remove('out');
                    player.node.count.innerHTML = '0';
                    player.node.hp.hide();
                    player.node.equips.hide();
                    player.node.count.hide();
                    player.previous.next = player.next;
                    player.next.previous = player.previous;
                    game.players.remove(player);
                    game.dead.push(player);
                    _status.dying.remove(player);

                    if (lib.config.background_speak) {
                        if (lib.character[player.name] && lib.character[player.name][4].contains('die_audio')) {
                            game.playAudio('die', player.name);
                        }
                        else if (lib.character[player.name] && lib.character[player.name][4].some(item => item.match(/^die:/))) {
                            let index = lib.character[player.name][4].findIndex(item => {
                                if (typeof item == "string") {
                                    return item.match(/^die:/) != null;
                                }
                                return false;
                            });
                            let audio = lib.character[player.name][4][index]
                                .replace(/^die:/, "")
                                .replace(/ext:/, "extension/");
                            game.playAudio('..', audio, player.name);
                        }
                        else if (lib.character[player.name] && lib.character[player.name][4].some(item => item.match(/^died:/))) {
                            let index = lib.character[player.name][4].findIndex(item => {
                                if (typeof item == "string") {
                                    return item.match(/^died:/) != null;
                                }
                                return false;
                            });
                            let audio = lib.character[player.name][4][index]
                                .replace(/^died:/, "")
                                .replace(/ext:/, "extension/");
                            game.playAudio('..', audio);
                        }
                        else {
                            game.playAudio('die', player.name, function () {
                                game.playAudio('die', player.name.slice(player.name.indexOf('_') + 1));
                            });
                        }
                    }
                }, player);

                game.addVideo('diex', player);
                if (event.animate !== false) {
                    player.$die(source);
                }
                if (player.hp != 0) {
                    player.changeHp(0 - player.hp, false).forceDie = true;
                }
                "step 1"
                if (player.dieAfter) player.dieAfter(source);
                "step 2"
                event.trigger('die');
                "step 3"
                if (player.isDead()) {
                    if (!game.reserveDead) {
                        for (var mark in player.marks) {
                            player.unmarkSkill(mark);
                        }
                        while (player.node.marks.childNodes.length > 1) {
                            player.node.marks.lastChild.remove();
                        }
                        game.broadcast(function (player) {
                            while (player.node.marks.childNodes.length > 1) {
                                player.node.marks.lastChild.remove();
                            }
                        }, player);
                    }
                    for (var i in player.tempSkills) {
                        player.removeSkill(i);
                    }
                    var skills = player.getSkills();
                    for (var i = 0; i < skills.length; i++) {
                        if (lib.skill[skills[i]].temp) {
                            player.removeSkill(skills[i]);
                        }
                    }
                    if (_status.characterlist) {
                        if (lib.character[player.name]) _status.characterlist.add(player.name);
                        if (lib.character[player.name1]) _status.characterlist.add(player.name1);
                        if (lib.character[player.name2]) _status.characterlist.add(player.name2);
                    }
                    event.cards = player.getCards('hejs');
                    if (event.cards.length) {
                        player.discard(event.cards).forceDie = true;
                        //player.$throw(event.cards,1000);
                    }
                }
                "step 4"
                if (player.dieAfter2) player.dieAfter2(source);
                "step 5"
                game.broadcastAll(function (player) {
                    if (game.online && player == game.me && !_status.over && !game.controlOver && !ui.exit) {
                        if (lib.mode[lib.configOL.mode].config.dierestart) {
                            ui.create.exit();
                        }
                    }
                }, player);
                if (!_status.connectMode && player == game.me && !_status.over && !game.controlOver) {
                    ui.control.show();
                    if (get.config('revive') && lib.mode[lib.config.mode].config.revive && !ui.revive) {
                        ui.revive = ui.create.control('revive', ui.click.dierevive);
                    }
                    if (get.config('continue_game') && !ui.continue_game && lib.mode[lib.config.mode].config.continue_game && !_status.brawl && !game.no_continue_game) {
                        ui.continue_game = ui.create.control('再战', game.reloadCurrent);
                    }
                    if (get.config('dierestart') && lib.mode[lib.config.mode].config.dierestart && !ui.restart) {
                        ui.restart = ui.create.control('restart', game.reload);
                    }
                }

                if (!_status.connectMode && player == game.me && !game.modeSwapPlayer) {
                    // _status.auto=false;
                    if (ui.auto) {
                        // ui.auto.classList.remove('glow');
                        ui.auto.hide();
                    }
                    if (ui.wuxie) ui.wuxie.hide();
                }

                if (typeof _status.coin == 'number' && source && !_status.auto) {
                    if (source == game.me || source.isUnderControl()) {
                        _status.coin += 10;
                    }
                }
                if (source && lib.config.border_style == 'auto' && (lib.config.autoborder_count == 'kill' || lib.config.autoborder_count == 'mix')) {
                    switch (source.node.framebg.dataset.auto) {
                        case 'gold': case 'silver': source.node.framebg.dataset.auto = 'gold'; break;
                        case 'bronze': source.node.framebg.dataset.auto = 'silver'; break;
                        default: source.node.framebg.dataset.auto = lib.config.autoborder_start || 'bronze';
                    }
                    if (lib.config.autoborder_count == 'kill') {
                        source.node.framebg.dataset.decoration = source.node.framebg.dataset.auto;
                    }
                    else {
                        var dnum = 0;
                        for (var j = 0; j < source.stat.length; j++) {
                            if (source.stat[j].damage != undefined) dnum += source.stat[j].damage;
                        }
                        source.node.framebg.dataset.decoration = '';
                        switch (source.node.framebg.dataset.auto) {
                            case 'bronze': if (dnum >= 4) source.node.framebg.dataset.decoration = 'bronze'; break;
                            case 'silver': if (dnum >= 8) source.node.framebg.dataset.decoration = 'silver'; break;
                            case 'gold': if (dnum >= 12) source.node.framebg.dataset.decoration = 'gold'; break;
                        }
                    }
                    source.classList.add('topcount');
                }
            };
        };
        // New Area
        const newArea = () => {
            _status.publicArea = ui.create.div('#publicArea');

            let publicPlayerFunction = {
                loseToPublicArea: function () {
                    var next = game.createEvent('loseToPublicArea');
                    next.player = this;
                    next.num = 0;
                    for (var i = 0; i < arguments.length; i++) {
                        if (get.itemtype(arguments[i]) == "player") {
                            next.source = arguments[i];
                        }
                        else if (get.itemtype(arguments[i]) == "cards") {
                            next.cards = arguments[i].slice(0);
                        }
                        else if (get.itemtype(arguments[i]) == "card") {
                            next.cards = [arguments[i]];
                        }
                        else if (typeof arguments[i] == "boolean") {
                            next.animate = arguments[i];
                        }
                        else if (arguments[i] == "notBySelf") {
                            next.notBySelf = true;
                        }
                    }
                    if (next.cards == undefined) _status.event.next.remove(next);
                    next.setContent("loseToPublicArea");
                    return next;
                }
            }

            let publicContentFunction = {
                loseToPublicArea: function () {
                    "step 0"
                    game.log(player, '将', cards, '置入了公共区域');
                    event.done = player.lose(cards, _status.publicArea, 'visible');
                    event.done.type = 'loseToPublicArea';
                    "step 1"
                    event.trigger('loseToPublicArea');
                }
            }

            lib.standard.overrides(lib.element.player, publicPlayerFunction);
            lib.standard.overrides(lib.element.content, publicContentFunction);
        };
        // Small Methods
        const newMethods = () => {
            // AsyncDamage
            {
                game.asyncDamage = function (players, num, ...options) {
                    if (!players) return;
                    let item = {};
                    options.forEach(option => {
                        if (typeof option === "string") {
                            item.nature = option;
                        }
                        else if (typeof option === "function") {
                            item.afterDo = option;
                        }
                        else if (get.itemtype(option) === "player") {
                            item.source = option;
                        }
                    });
                    for (var i = 0; i < players.length; i++) {
                        let value = 1;
                        if (typeof num === "number") {
                            value = num;
                        }
                        else if (Array.isArray(num)) {
                            value = num[i];
                        }
                        else if (typeof num == 'function') {
                            value = num(players[i]);
                        }
                        let args = Array.new();
                        if (item.nature) args.push(item.nature);
                        if (item.source) args.push(item.source);
                        let next = players[i].damage(value, ...args);
                        if (!item.source) next.set("source", undefined);
                        if (item.afterDo) item.afterDo(next);
                    }
                };

                lib.element.player.asyncDamage = function (players, num, ...options) {
                    return game.asyncDamage(players, num, ...options, this);
                }
            }
        };
        // Over Modify
        const over = () => {
            game.over = function (result) {
                if (_status.over) return;
                if (game.me._trueMe) game.swapPlayer(game.me._trueMe);
                var i, j, k, num, table, tr, td, dialog;
                _status.over = true;
                ui.control.show();
                ui.clear();
                game.stopCountChoose();
                if (ui.time3) {
                    clearInterval(ui.time3.interval);
                }
                if ((game.layout == 'long2' || game.layout == 'nova') && !game.chess) {
                    ui.arena.classList.add('choose-character');
                    ui.me.hide();
                    ui.mebg.hide()
                    ui.autonode.hide();
                    if (lib.config.radius_size != 'off') {
                        ui.historybar.style.borderRadius = '0 0 0 4px';
                    }
                }
                if (game.online) {
                    var dialog = ui.create.dialog();
                    dialog.noforcebutton = true;
                    dialog.content.innerHTML = result;
                    dialog.forcebutton = true;
                    var result2 = arguments[1];
                    if (result2 == true) {
                        dialog.content.firstChild.innerHTML = '战斗胜利';
                    }
                    else if (result2 == false) {
                        dialog.content.firstChild.innerHTML = '战斗失败';
                    }
                    ui.update();
                    dialog.add(ui.create.div('.placeholder'));
                    for (var i = 0; i < game.players.length; i++) {
                        var hs = game.players[i].getCards('h');
                        if (hs.length) {
                            dialog.add('<div class="text center">' + get.translation(game.players[i]) + '</div>');
                            dialog.addSmall(hs);
                        }
                    }

                    for (var j = 0; j < game.dead.length; j++) {
                        var hs = game.dead[j].getCards('h');
                        if (hs.length) {
                            dialog.add('<div class="text center">' + get.translation(game.dead[j]) + '</div>');
                            dialog.addSmall(hs);
                        }
                    }

                    dialog.add(ui.create.div('.placeholder.slim'));
                    if (lib.config.background_audio) {
                        if (result2 === true) {
                            game.playAudio('effect', 'win');
                        }
                        else if (result2 === false) {
                            game.playAudio('effect', 'lose');
                        }
                        else {
                            game.playAudio('effect', 'tie');
                        }
                    }
                    if (!ui.exit) {
                        ui.create.exit();
                    }
                    if (ui.giveup) {
                        ui.giveup.remove();
                        delete ui.giveup;
                    }
                    if (game.servermode) {
                        ui.exit.firstChild.innerHTML = '返回房间';
                        setTimeout(function () {
                            ui.exit.firstChild.innerHTML = '退出房间';
                            _status.roomtimeout = true;
                            lib.config.reconnect_info[2] = null;
                            game.saveConfig('reconnect_info', lib.config.reconnect_info);
                        }, 10000);
                    }
                    if (ui.tempnowuxie) {
                        ui.tempnowuxie.close();
                        delete ui.tempnowuxie;
                    }
                    if (ui.auto) ui.auto.hide();
                    if (ui.wuxie) ui.wuxie.hide();
                    if (game.getIdentityList) {
                        for (var i = 0; i < game.players.length; i++) {
                            game.players[i].setIdentity();
                        }
                    }
                    return;
                }
                if (lib.config.background_audio) {
                    if (result === true) {
                        game.playAudio('effect', 'win');
                    }
                    else if (result === false) {
                        game.playAudio('effect', 'lose');
                    }
                    else {
                        game.playAudio('effect', 'tie');
                    }
                }
                var resultbool = result;
                if (typeof resultbool !== 'boolean') {
                    resultbool = null;
                }
                if (result === true) result = '战斗胜利';
                if (result === false) result = '战斗失败';
                if (result == undefined) result = '战斗结束';
                dialog = ui.create.dialog(result);
                dialog.noforcebutton = true;
                dialog.forcebutton = true;
                if (game.addOverDialog) {
                    game.addOverDialog(dialog, result);
                }
                if (typeof _status.coin == 'number' && !_status.connectMode) {
                    var coeff = Math.random() * 0.4 + 0.8;
                    var added = 0;
                    var betWin = false;
                    if (result == '战斗胜利') {
                        if (_status.betWin) {
                            betWin = true;
                            _status.coin += 10;
                        }
                        _status.coin += 20;
                        if (_status.additionalReward) {
                            _status.coin += _status.additionalReward();
                        }
                        switch (lib.config.mode) {
                            case 'identity': {
                                switch (game.me.identity) {
                                    case 'zhu': case 'zhong': case 'mingzhong':
                                        if (get.config('enhance_zhu')) {
                                            added = 10;
                                        }
                                        else {
                                            added = 20;
                                        }
                                        break;
                                    case 'fan':
                                        if (get.config('enhance_zhu')) {
                                            added = 16;
                                        }
                                        else {
                                            added = 8;
                                        }
                                        break;
                                    case 'nei':
                                        added = 40;
                                        break;
                                }
                                added = added * (game.players.length + game.dead.length) / 8;
                                break;
                            }
                            case 'guozhan':
                                if (game.me.identity == 'ye') {
                                    added = 8;
                                }
                                else {
                                    added = 5 / get.totalPopulation(game.me.identity);
                                }
                                added = added * (game.players.length + game.dead.length);
                                break;
                            case 'versus':
                                if (_status.friend) {
                                    added = 5 * (game.players.length + _status.friend.length);
                                }
                                break;
                            default:
                                added = 10;
                        }
                    }
                    else {
                        added = 10;
                    }
                    if (lib.config.mode == 'chess' && _status.mode == 'combat' && get.config('additional_player')) {
                        added = 2;
                    }
                    _status.coin += added * coeff;
                    if (_status.coinCoeff) {
                        _status.coin *= _status.coinCoeff;
                    }
                    _status.coin = Math.ceil(_status.coin);
                    dialog.add(ui.create.div('', '获得' + _status.coin + '金'));
                    if (betWin) {
                        game.changeCoin(20);
                        dialog.content.appendChild(document.createElement('br'));
                        dialog.add(ui.create.div('', '（下注赢得10金）'));
                    }
                    game.changeCoin(_status.coin);
                }
                if (get.mode() == 'versus' && _status.ladder) {
                    var mmr = _status.ladder_mmr;
                    mmr += 10 - get.rank(game.me.name, true) * 2;
                    if (result == '战斗胜利') {
                        mmr = 20 + Math.round(mmr);
                        if (mmr > 40) {
                            mmr = 40;
                        }
                        else if (mmr < 10) {
                            mmr = 10;
                        }
                        dialog.add(ui.create.div('', '获得 ' + mmr + ' 积分'));
                    }
                    else {
                        mmr = -30 + Math.round(mmr / 2);
                        if (mmr > -20) {
                            mmr = -20;
                        }
                        else if (mmr < -35) {
                            mmr = -35;
                        }
                        if (lib.storage.ladder.current < 900) {
                            mmr = Math.round(mmr / 4);
                        }
                        else if (lib.storage.ladder.current < 1400) {
                            mmr = Math.round(mmr / 2);
                        }
                        else if (lib.storage.ladder.current < 2000) {
                            mmr = Math.round(mmr / 1.5);
                        }
                        else if (lib.storage.ladder.current > 2500) {
                            mmr = Math.round(mmr * 1.5);
                        }
                        dialog.add(ui.create.div('', '失去 ' + (-mmr) + ' 积分'));
                    }
                    if (_status.ladder_tmp) {
                        lib.storage.ladder.current += 40;
                        delete _status.ladder_tmp;
                    }
                    lib.storage.ladder.current += mmr;
                    if (lib.storage.ladder.top < lib.storage.ladder.current) {
                        lib.storage.ladder.top = lib.storage.ladder.current;
                    }
                    game.save('ladder', lib.storage.ladder);
                    if (ui.ladder && game.getLadderName) {
                        ui.ladder.innerHTML = game.getLadderName(lib.storage.ladder.current);
                    }
                }
                // if(true){
                if (game.players.length) {
                    table = document.createElement('table');
                    tr = document.createElement('tr');
                    tr.appendChild(document.createElement('td'));
                    td = document.createElement('td');
                    td.innerHTML = '伤害';
                    tr.appendChild(td);
                    td = document.createElement('td');
                    td.innerHTML = '受伤';
                    tr.appendChild(td);
                    td = document.createElement('td');
                    td.innerHTML = '摸牌';
                    tr.appendChild(td);
                    td = document.createElement('td');
                    td.innerHTML = '出牌';
                    tr.appendChild(td);
                    td = document.createElement('td');
                    td.innerHTML = '杀敌';
                    tr.appendChild(td);
                    table.appendChild(tr);
                    for (i = 0; i < game.players.length; i++) {
                        tr = document.createElement('tr');
                        td = document.createElement('td');
                        td.innerHTML = get.translation(game.players[i]);
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.players[i].stat.length; j++) {
                            if (game.players[i].stat[j].damage != undefined) num += game.players[i].stat[j].damage;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.players[i].stat.length; j++) {
                            if (game.players[i].stat[j].damaged != undefined) num += game.players[i].stat[j].damaged;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.players[i].stat.length; j++) {
                            if (game.players[i].stat[j].gain != undefined) num += game.players[i].stat[j].gain;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.players[i].stat.length; j++) {
                            for (k in game.players[i].stat[j].card) {
                                num += game.players[i].stat[j].card[k];
                            }
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.players[i].stat.length; j++) {
                            if (game.players[i].stat[j].kill != undefined) num += game.players[i].stat[j].kill;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        table.appendChild(tr);
                    }
                    dialog.add(ui.create.div('.placeholder'));
                    dialog.content.appendChild(table);
                }
                if (game.dead.length) {
                    table = document.createElement('table');
                    table.style.opacity = '0.5';
                    if (game.players.length == 0) {
                        tr = document.createElement('tr');
                        tr.appendChild(document.createElement('td'));
                        td = document.createElement('td');
                        td.innerHTML = '伤害';
                        tr.appendChild(td);
                        td = document.createElement('td');
                        td.innerHTML = '受伤';
                        tr.appendChild(td);
                        td = document.createElement('td');
                        td.innerHTML = '摸牌';
                        tr.appendChild(td);
                        td = document.createElement('td');
                        td.innerHTML = '出牌';
                        tr.appendChild(td);
                        td = document.createElement('td');
                        td.innerHTML = '杀敌';
                        tr.appendChild(td);
                        table.appendChild(tr);
                    }
                    for (i = 0; i < game.dead.length; i++) {
                        tr = document.createElement('tr');
                        td = document.createElement('td');
                        td.innerHTML = get.translation(game.dead[i]);
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.dead[i].stat.length; j++) {
                            if (game.dead[i].stat[j].damage != undefined) num += game.dead[i].stat[j].damage;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.dead[i].stat.length; j++) {
                            if (game.dead[i].stat[j].damaged != undefined) num += game.dead[i].stat[j].damaged;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.dead[i].stat.length; j++) {
                            if (game.dead[i].stat[j].gain != undefined) num += game.dead[i].stat[j].gain;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.dead[i].stat.length; j++) {
                            for (k in game.dead[i].stat[j].card) {
                                num += game.dead[i].stat[j].card[k];
                            }
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.dead[i].stat.length; j++) {
                            if (game.dead[i].stat[j].kill != undefined) num += game.dead[i].stat[j].kill;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        table.appendChild(tr);
                    }
                    dialog.add(ui.create.div('.placeholder'));
                    dialog.content.appendChild(table);
                }
                if (game.additionaldead && game.additionaldead.length) {
                    table = document.createElement('table');
                    table.style.opacity = '0.5';
                    for (i = 0; i < game.additionaldead.length; i++) {
                        tr = document.createElement('tr');
                        td = document.createElement('td');
                        td.innerHTML = get.translation(game.additionaldead[i]);
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.additionaldead[i].stat.length; j++) {
                            if (game.additionaldead[i].stat[j].damage != undefined) num += game.additionaldead[i].stat[j].damage;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.additionaldead[i].stat.length; j++) {
                            if (game.additionaldead[i].stat[j].damaged != undefined) num += game.additionaldead[i].stat[j].damaged;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.additionaldead[i].stat.length; j++) {
                            if (game.additionaldead[i].stat[j].gain != undefined) num += game.additionaldead[i].stat[j].gain;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.additionaldead[i].stat.length; j++) {
                            for (k in game.additionaldead[i].stat[j].card) {
                                num += game.additionaldead[i].stat[j].card[k];
                            }
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        td = document.createElement('td');
                        num = 0;
                        for (j = 0; j < game.additionaldead[i].stat.length; j++) {
                            if (game.additionaldead[i].stat[j].kill != undefined) num += game.additionaldead[i].stat[j].kill;
                        }
                        td.innerHTML = num;
                        tr.appendChild(td);
                        table.appendChild(tr);
                    }
                    dialog.add(ui.create.div('.placeholder'));
                    dialog.content.appendChild(table);
                }
                // }
                dialog.add(ui.create.div('.placeholder'));

                var clients = game.players.concat(game.dead);
                for (var i = 0; i < clients.length; i++) {
                    if (clients[i].isOnline2()) {
                        clients[i].send(game.over, dialog.content.innerHTML, game.checkOnlineResult(clients[i]));
                    }
                }

                dialog.add(ui.create.div('.placeholder'));

                for (var i = 0; i < game.players.length; i++) {
                    // if (!_status.connectMode /* && game.players[i].isUnderControl(true) */ && game.layout != "long2") continue;
                    var hs = game.players[i].getCards('h');
                    if (hs.length) {
                        dialog.add('<div class="text center">' + get.translation(game.players[i]) + '</div>');
                        dialog.addSmall(hs);
                    }
                }
                for (var i = 0; i < game.dead.length; i++) {
                    // if (!_status.connectMode /* && game.dead[i].isUnderControl(true) */ && game.layout != 'long2') continue;
                    var hs = game.dead[i].getCards('h');
                    if (hs.length) {
                        dialog.add('<div class="text center">' + get.translation(game.dead[i]) + '</div>');
                        dialog.addSmall(hs);
                    }
                }
                dialog.add(ui.create.div('.placeholder.slim'));
                game.addVideo('over', null, dialog.content.innerHTML);
                var vinum = parseInt(lib.config.video);
                if (!_status.video && vinum && game.getVideoName && window.indexedDB && _status.videoInited) {
                    var store = lib.db.transaction(['video'], 'readwrite').objectStore('video');
                    var videos = lib.videos.slice(0);
                    for (var i = 0; i < videos.length; i++) {
                        if (videos[i].starred) {
                            videos.splice(i--, 1);
                        }
                    }
                    for (var deletei = 0; deletei < 5; deletei++) {
                        if (videos.length >= vinum) {
                            var toremove = videos.pop();
                            lib.videos.remove(toremove);
                            store.delete(toremove.time);
                        }
                        else {
                            break;
                        }
                    }
                    var me = game.me || game.players[0];
                    if (!me) return;
                    var newvid = {
                        name: game.getVideoName(),
                        mode: lib.config.mode,
                        video: lib.video,
                        win: result == '战斗胜利',
                        name1: me.name1 || me.name,
                        name2: me.name2,
                        time: lib.getUTC(new Date())
                    };
                    var modecharacters = lib.characterPack['mode_' + get.mode()];
                    if (modecharacters) {
                        if (get.mode() == 'guozhan') {
                            if (modecharacters[newvid.name1]) {
                                if (newvid.name1.indexOf('gz_shibing') == 0) {
                                    newvid.name1 = newvid.name1.slice(3, 11);
                                }
                                else {
                                    newvid.name1 = newvid.name1.slice(3);
                                }
                            }
                            if (modecharacters[newvid.name2]) {
                                if (newvid.name2.indexOf('gz_shibing') == 0) {
                                    newvid.name2 = newvid.name2.slice(3, 11);
                                }
                                else {
                                    newvid.name2 = newvid.name2.slice(3);
                                }
                            }
                        }
                        else {
                            if (modecharacters[newvid.name1]) {
                                newvid.name1 = get.mode() + '::' + newvid.name1;
                            }
                            if (modecharacters[newvid.name2]) {
                                newvid.name2 = get.mode() + '::' + newvid.name2;
                            }
                        }
                    }
                    if (newvid.name1 && newvid.name1.indexOf('subplayer_') == 0) {
                        newvid.name1 = newvid.name1.slice(10, newvid.name1.lastIndexOf('_'));
                    }
                    if (newvid.name2 && newvid.name2.indexOf('subplayer_') == 0) {
                        newvid.name1 = newvid.name2.slice(10, newvid.name1.lastIndexOf('_'));
                    }
                    lib.videos.unshift(newvid);
                    store.put(newvid);
                    ui.create.videoNode(newvid, true);
                }
                // _status.auto=false;
                if (ui.auto) {
                    // ui.auto.classList.remove('glow');
                    ui.auto.hide();
                }
                if (ui.wuxie) ui.wuxie.hide();
                if (ui.giveup) {
                    ui.giveup.remove();
                    delete ui.giveup;
                }

                if (lib.config.test_game && !_status.connectMode) {
                    if (typeof lib.config.test_game !== 'string') {
                        switch (lib.config.mode) {
                            case 'identity': game.saveConfig('mode', 'guozhan'); break;
                            case 'guozhan': game.saveConfig('mode', 'versus'); break;
                            case 'versus': game.saveConfig('mode', 'boss'); break;
                            case 'boss': game.saveConfig('mode', 'chess'); break;
                            case 'chess': game.saveConfig('mode', 'stone'); break;
                            case 'stone': game.saveConfig('mode', 'identity'); break;
                        }
                    }
                    setTimeout(game.reload, 500);
                }
                if (game.controlOver) {
                    game.controlOver(); return;
                }
                if (!_status.brawl) {
                    if (lib.config.mode == 'boss') {
                        ui.create.control('再战', function () {
                            var pointer = game.boss;
                            var map = { boss: game.me == game.boss, links: [] };
                            for (var iwhile = 0; iwhile < 10; iwhile++) {
                                pointer = pointer.nextSeat;
                                if (pointer == game.boss) {
                                    break;
                                }
                                if (!pointer.side) {
                                    map.links.push(pointer.name);
                                }
                            }
                            game.saveConfig('continue_name_boss', map);
                            game.saveConfig('mode', lib.config.mode);
                            localStorage.setItem(lib.configprefix + 'directstart', true);
                            game.reload();
                        });
                    }
                    else if (lib.config.mode == 'versus') {
                        if (_status.mode == 'standard' || _status.mode == 'three') {
                            ui.create.control('再战', function () {
                                game.saveConfig('continue_name_versus' + (_status.mode == 'three' ? '_three' : ''), {
                                    friend: _status.friendBackup,
                                    enemy: _status.enemyBackup,
                                    color: _status.color
                                });
                                game.saveConfig('mode', lib.config.mode);
                                localStorage.setItem(lib.configprefix + 'directstart', true);
                                game.reload();
                            });
                        }
                    }
                    else if (!_status.connectMode && get.config('continue_game') && !ui.continue_game && !_status.brawl && !game.no_continue_game) {
                        ui.continue_game = ui.create.control('再战', game.reloadCurrent);
                    }
                }
                if (!ui.restart) {
                    if (game.onlineroom && typeof game.roomId == 'string') {
                        ui.restart = ui.create.control('restart', function () {
                            game.broadcastAll(function () {
                                if (ui.exit) {
                                    ui.exit.stay = true;
                                    ui.exit.firstChild.innerHTML = '返回房间';
                                }
                            });
                            game.saveConfig('tmp_owner_roomId', game.roomId);
                            setTimeout(game.reload, 100);
                        });
                    }
                    else {
                        ui.restart = ui.create.control('restart', game.reload);
                    }
                }
                if (ui.tempnowuxie) {
                    ui.tempnowuxie.close();
                    delete ui.tempnowuxie;
                }

                if (ui.revive) {
                    ui.revive.close();
                    delete ui.revive;
                }
                if (ui.swap) {
                    ui.swap.close();
                    delete ui.swap;
                }
                for (var i = 0; i < lib.onover.length; i++) {
                    lib.onover[i](resultbool);
                }
                if (game.addRecord) {
                    game.addRecord(resultbool);
                }
                if (window.isNonameServer) {
                    lib.configOL.gameStarted = false;
                    game.saveConfig('pagecfg' + window.isNonameServer, [lib.configOL, game.roomId, _status.onlinenickname, _status.onlineavatar]);
                    game.reload();
                }
                else if (_status.connectMode && !game.online) {
                    setTimeout(game.reload, 15000)
                }
            };
        };
        // Translation
        {
            lib.translate["visualCard"] = "明示";
        }
        // Do
        const list = [newGet, newPlayer, newElement, mutilPlayers, nodeIntro, alias, visualMethods, dieAudio, newArea, newMethods, over];
        list.forEach(item => {
            isInit ? lib.arenaReady.push(item) : item();
        })
    };
    methods();
    // lib.arenaReady.push(methods);
}