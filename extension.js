// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From rExtension System
// Licensed under BSD-2-Caluse
// File: extension.js (rExtension/extension-winloud/extension.js)
// Content:  风轻云淡 扩展主文件
// Copyright (c) 2022 rExtension System All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

game.import("extension", (lib, game, ui, get, ai, _status) => {
    const extInfo = {
		intro: "三国时代覆灭后，某位五胡乱华中幸存的一位士兵捡到了破损的太平天书......随后意外重启了三国！但是，这次的三国，好像不太一样......",
		version: "0.1.0",
		branch: "Master",
		build: 1,
		year: 2022,
		month: "07",
		date: "28",
		times: "002",
	};
    return {
        name: "风轻云淡", 
        editable: false,
        content: (config, _pack) => {
            // Load Extension
			// 读取扩展主要信息
			lib.arenaReady.push(() => {
				//
				if (!Map.new) {
					Map.new = function () {
						return new Map(...arguments);
					}
				}
				// Define information
				// 定义变量
				const translate = "风轻云淡";
				const extension = "winloud";
				const type = "rExtension";
				const env = Map.new();

				if (!lib.rExtension || !lib.rExtension["StandardLib"]) {
					require(`./extension/${translate}/lib/StandardLib/module.js`)(lib, game, ui, get, ai, _status);
				}

				if (lib.rExtension["StandardLib"].lib) lib.rExtension["StandardLib"].lib.refresh();

				// 设置二级介绍
				let intro2 = [
                    "<span style=\"color:#1688F2\">contents from rExtension",
					"Authors:",
                    "- Rintim &lt;rintim@foxmail.com&gt;",
					"- Angel &lt;3371152010@qq.com&gt;",
					`Version: ${["Release", "Preview"].includes(extInfo.branch) ?
						(extInfo.branch === "Release" ? extInfo.version : `${extInfo.version}pre`)
						: (extInfo.branch === "Development" ?
							`Build ${extInfo.year}.${extInfo.month}.${extInfo.date}.${extInfo.times}`
							: `Build ${extInfo.build}${extInfo.nextPreview === null ? "" : ` Form ${extInfo.nextPreview}`}`)
					}`,
					"本扩展目前为测试阶段，欢迎反馈BUG</span>",
					"- - - - - - - - - - - - - - - - - - - - - - - - -",
					"●仓库地址: ",
					"- Gitlab:/rExtension/extension-winloud",
                    ""
				]
				if (extInfo.branch === "Development") {
					intro2.addArray([
						"●注意: <span style=\"color:#FF3333\">你目前在使用Development分支",
						"该分支下可能会出现BUG，请确认需求使用</span>",
						""
					])
				}

				const dot = config.webp_enable ? ".webp" : ".png";
				
				// Set Update Strings
				// 设置更新日志
				{
					lib.extensionPack[translate].version = extInfo.version;
					/*
					if (!lib.config[`rExtension_config_${extension}_info`]) game.saveConfig(`rExtension_config_${extension}_info`, undefined);
					if (!lib.config[`rExtension_config_${extension}_des_bool`]) game.saveConfig(`rExtension_config_${extension}_des_bool`, false);
					if (!lib.config[`rExtension_config_${extension}_log_bool`] || lib.config[`rExtension_config_${extension}_info`] !== extInfo) {
						game.saveConfig(`rExtension_config_${extension}_log_bool`, false);
						game.saveConfig(`rExtension_config_${extension}_info`, extInfo);
					}
					*/
					let strings = [
						"</li>",
						"注意：",
						"本扩展目前处于测试阶段，可能会有一些未知BUG",
						"如发现BUG，希望能及时反馈",
                        "（第一次加载扩展不会加载武将，重启无名杀后方可加载）"
					];
					game.showExtensionChangeLog(strings.join("</br>"), translate);
				}
				// Set Menu
				// 设置扩展设定菜单
				{
					let _delete = lib.extensionMenu[`extension_${translate}`].delete;

					delete lib.extensionMenu[`extension_${translate}`].delete;
                    
                    lib.extensionMenu[`extension_${translate}`].intro.name = `<img src=\"${lib.standard.path("extension", translate, "res", "image", "splash", `title${dot}`)}\" alt=\"风轻云淡\"style=\"width:100%;text-align:center;\"onclick=\"if (lib.jlsg) lib.jlsg.showRepoElement(this)\"></img>三国时代覆灭后，某位五胡乱华中幸存的一位士兵捡到了破损的太平天书......随后意外重启了三国！但是，这次的三国，好像不太一样......`

					delete lib.extensionMenu[`extension_${translate}`].author;

					lib.extensionMenu[`extension_${translate}`].des = {
						name: `<div class="${type}_${extension}">扩展介绍<font size="5px" ${!lib.config[`rExtension_config_${extension}_des_bool`] ? "color='gold'" : ""}>⇨</font></div>`,
						clear: true,
						onclick: function () {
							if (this[`${type}_${extension}_Intro_Click`] === undefined) {
								let more = ui.create.div(`.${type}_${extension}_Click`, [
									"<div style=\"border: 1px solid gray\"><font size=2px>To Do",
									"本扩展重在设计，如有好的设计也欢迎提供</font></div>"
								].join("</br>"))
								this.parentNode.insertBefore(more, this.nextSibling);
								this[`${type}_${extension}_Intro_Click`] = more;
								if (!lib.config[`rExtension_config_${extension}_des_bool`]) lib.config[`rExtension_config_${extension}_des_bool`] = true;
								this.innerHTML = `<div class="${type}_${extension}">扩展介绍<font size="5px">⇩</font></div>`;
							} else {
								this.parentNode.removeChild(this[`${type}_${extension}_Intro_Click`]);
								delete this[`${type}_${extension}_Intro_Click`];
								this.innerHTML = `<div class="${type}_${extension}">扩展介绍<font size="5px">⇨</font></div>`;
							};
						}
					}

					lib.extensionMenu[`extension_${translate}`].intro2 = {
						name: intro2.join("</br>"),
						clear: true,
						nopointer: true,
					}

					lib.extensionMenu[`extension_${translate}`].changeLog = {
						name: `<div class="${type}_${extension}">更新日志<font size="5px" ${!lib.config[`rExtension_config_${extension}_log_bool`] ? "color='gold'" : ""}>⇨</font></div>`,
						clear: true,
						onclick: function () {
							if (this[`${type}_${extension}_Log_Click`] === undefined) {
								let more = ui.create.div(`.${type}_${extension}_Click`, [
									"<div style=\"border: 1px solid gray\"><font size=2px>跨越0.1.0</font></div>"
								].join("</br>"))
								this.parentNode.insertBefore(more, this.nextSibling);
								this[`${type}_${extension}_Log_Click`] = more;
								if (!lib.config[`rExtension_config_${extension}_log_bool`]) lib.config[`rExtension_config_${extension}_log_bool`] = true;
								this.innerHTML = `<div class="${type}_${extension}">更新日志<font size="5px">⇩</font></div>`;
							} else {
								this.parentNode.removeChild(this[`${type}_${extension}_Log_Click`]);
								delete this[`${type}_${extension}_Log_Click`];
								this.innerHTML = `<div class="${type}_${extension}">更新日志<font size="5px">⇨</font></div>`;
							};
						}
					}

					lib.extensionMenu[`extension_${translate}`]["config_title"] = {
						"name": "<p align=center><span style=\"font-size:18px\">- - - - - - - 武将设置 - - - - - - -</span>",
						"clear": true,
						"nopointer": true
					};

					lib.extensionMenu[`extension_${translate}`]["character_enable"] = {
						name: "武将启用",
						intro: "开启此功能后重启生效。启用扩展包中的武将。",
						init: true,
					};

					lib.extensionMenu[`extension_${translate}`]["webp_enable"] = {
						name: "低耗模式",
						intro: "开启此功能后重启生效。",
						init: false,
					};

					lib.extensionMenu[`extension_${translate}`]["colour_enable"] = {
						name: "高亮描述",
						intro: "（WIP）开启此功能后重启生效。使用彩色描述，高亮重点标识",
						init: false,
					};

					lib.extensionMenu[`extension_${translate}`]["Last BR"] = {
						"name": "</br>",
						"clear": true,
						"nopointer": true
					};

					lib.extensionMenu[`extension_${translate}`].delete = _delete;
				}
				// 读取扩展内容
				{
					// 武将
					{
						const list = ["standard", "wind"];
						const characters = Map.new();
						const skills = Map.new();
						list.forEach(item => {
							const info = require(lib.standard.path("extension", translate, "src", "character", `${item}.js`))({translate, dot});
							for (const name in info.character) {
								if (!info.character[name].catelogy) info.character[name].catelogy = item;
								characters.set(name, info.character[name]);
							}
							for (const name in info.skill) skills.set(name, info.skill[name]);
						})
						const result = Map.new([["character", characters], ["skill", skills]]);
						env.set("character", result);
					}
				}
				// Add Skill
				// 添加扩展技能
				if (config.character_enable) {
					const skills = env.get("character").get("skill");

					for (let [skill, { name, des, desc, content }] of skills) {
						// let { name, des, desc, content } = skills[skill];
						if (content) {
							lib.skill[skill] = content;
							if (name) lib.translate[skill] = name;
							if (des || desc) lib.translate[`${skill}_info`] = config.colour_enable ? (desc ? desc : des) : des;
						}
					}
				}
				// Add Character
				// 添加扩展武将
				if (config.character_enable) {
					const characters = env.get("character").get("character"); // require(`./extension/${translate}/src/character.js`)({translate, dot});

					if (!lib.characterPack[`mode_extension_${type}_${extension}`]) lib.characterPack[`mode_extension_${type}_${extension}`] = {};

					for (const [name, info] of characters) {
						// const info = characters[name]
						let showHp;
						if (!info.maxHp || info.maxHp == info.hp) showHp = info.hp;
						else showHp = `${info.hp}/${info.maxHp}`;
						let character = [info.sex, info.group, showHp, info.skills, info.tags];
						lib.character[name] = character;
						lib.translate[name] = info.translate;
						lib.characterPack[`mode_extension_${type}_${extension}`][name] = character;
						if (info.rank) {
							if (typeof info.rank === "object") {
								lib.rank.rarity[info.rank["rarity"]].push(name);
							}
							else lib.rank[info.rank].push(name);
						}
						if (info.des) {
							lib.characterIntro[name] = info.des;
						}
						if (info.title) {
							lib.characterTitle[name] = info.title;
						}
						if (info.catelogy) {
							if (!lib.characterSort[`mode_extension_${type}_${extension}`]) lib.characterSort[`mode_extension_${type}_${extension}`] = {};
							if (!lib.characterSort[`mode_extension_${type}_${extension}`][`${type}_${extension}_${info.catelogy}`]) lib.characterSort[`mode_extension_${type}_${extension}`][`${type}_${extension}_${info.catelogy}`] = [];
							lib.characterSort[`mode_extension_${type}_${extension}`][`${type}_${extension}_${info.catelogy}`].push(name);
						}
						if (info.replace) {
							if (!lib.characterReplace[info.replace]) lib.characterReplace[info.replace] = [info.replace];
							if (!lib.characterReplace[info.replace].includes(name)) lib.characterReplace[info.replace].push(name);
						}
					}

					const catelogy = {
						"standard": "万物开端",
						"wind": "风驰电掣"
					}

					lib.translate[`mode_extension_${type}_${extension}_character_config`] = translate;
					lib.translate[`${type}_${extension}`] = translate;
					for (let name in catelogy) lib.translate[`${type}_${extension}_${name}`] = catelogy[name];
				}
			});
        }, 
        precontent: () => {

        }, 
        config: {},
        help: {}, 
        package: {
            intro: "三国时代覆灭后，某位五胡乱华中幸存的一位士兵捡到了破损的太平天书......随后意外重启了三国！但是，这次的三国，好像不太一样......",
            author: "rExtension",
            diskURL: "",
            forumURL: "",
            version: "0.1.0",
        }, files: { "character": [], "card": [], "skill": [] }
    }
})