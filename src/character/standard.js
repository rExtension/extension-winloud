// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =
// This File is From rExtension System
// Licensed under BSD-2-Caluse
// File: standard.js (rExtension/extension-winloud/source/character/standard.js)
// Content:  风轻云淡 万物开端栏武将信息
// Copyright (c) 2022 rExtension System All rights reserved
// = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = = =

"use strict"

module.exports = ({ translate, dot }) => ({
	character: {
		// 蜀/季汉
		"rExtension_rEyd-SHU001": {
			translate: "刘备",
			title: "末世英雄",
			sex: "male",
			group: "shu",
			hp: 4,
			maxHp: 4,
			rank: { rarity: "epic" },
			replace: "liubei",
			skills: ["rExtension_winloud_hongyi", "rExtension_winloud_renwang"],
			tags: [
				"zhu",
				`ext:${translate}/res/image/character/rEyd-SHU001${dot}`,
				`died:ext:${translate}/res/audio/die/rEyd-SHU001.ogg`,
			],
			des: "字玄德，幽州涿郡涿县（今河北省涿州市）人，亦称汉先主，是三国时代蜀汉政权的开创者暨第一位皇帝，在位三年。谥号昭烈皇帝，庙号烈祖。《三国志》、《华阳国志》等书均称其为先主，继位的刘禅则被称为“后主”，《资治通鉴》称刘备父子为汉主。",
			catelogy: "standard"
		},
		"rExtension_rEyd-SHU003": {
			translate: "张飞",
			title: "万夫莫开",
			sex: "male",
			group: "shu",
			hp: 4,
			maxHp: 4,
			rank: { rarity: "epic" },
			replace: "zhangfei",
			skills: ["rExtension_winloud_mangji", "rExtension_winloud_duanhe"],
			tags: [
				"zhu",
				`ext:${translate}/res/image/character/rEyd-SHU003${dot}`,
				`died:ext:${translate}/res/audio/die/rEyd-SHU003.ogg`,
			],
			des: "字益德，东汉末年幽州涿郡（今河北省保定市涿州市）人氏，三国时期蜀汉名将，亦是刘备的主要亲信之一，与关羽并称“万人敌”。官至车骑将军、领司隶校尉，封西乡侯，后遇刺身亡，蜀汉追谥桓侯，是为西乡桓侯。元朝加封为武义忠显英烈灵惠助顺王。陈寿在撰写《三国志》的时候，将张飞与关羽、马超、黄忠、赵云合为一传（《三国志·蜀书·关张马黄赵传》），罗贯中的长篇小说《三国演义》中又将该五人并称“五虎上将”，随着民间剧目的发展，便广为世人所知。而在中国传统文化中，张飞以粗犷刚烈好酒著称，虽然此形象主要来源于小说和戏剧等民间艺术，但已深入人心。",
			catelogy: "standard"
		},
		"rExtension_rEyd-SHU006": {
			translate: "马超",
			title: "一骑当千",
			sex: "male",
			group: "shu",
			hp: 4,
			maxHp: 4,
			rank: { rarity: "epic" },
			replace: "machao",
			skills: ["rExtension_winloud_tieji", "rExtension_winloud_zhuiji"],
			tags: [
				"zhu",
				`ext:${translate}/res/image/character/rEyd-SHU006${dot}`,
				`died:ext:${translate}/res/audio/die/rEyd-SHU006.ogg`,
			],
			des: "字孟起，扶风茂陵（今陕西省兴平东北）人，汉末三国时期蜀汉著名将领。刘备称帝后任骠骑将军，领凉州牧，进封斄乡侯。故后谥威侯，是为斄乡威侯。陈寿在撰写《三国志》的时候，将马超与关羽、张飞、黄忠、赵云合为一传（《三国志·蜀书·关张马黄赵传》），罗贯中的长篇小说《三国演义》中又将该五人并称“五虎上将”，广为世人所知。 ",
			catelogy: "standard"
		},
		// 曹魏
		// Draw Pie
		/*
		"rExtension_rEyd-WEI001": {
			translate: "曹操",
			title: "乱世奸雄",
			sex: "male",
			group: "wei",
			hp: 4,
			maxHp: 4,
			rank: { rarity: "epic" },
			replace: "caocao",
			skills: ["rExtension_winloud_jianxiong" , "rExtension_winloud_guixin" ],
			tags: [
				"zhu",
				`ext:${translate}/res/image/character/rEyd-WEI001${dot}`,
				`died:ext:${translate}/res/audio/die/rEyd-WEI001.ogg`,
			],
			des: "字孟德，小名吉利，小字阿瞒，沛国谯县（今安徽亳州）人。东汉末年著名的权臣、军事家、政治家、文学家和诗人，三国时代曹魏奠基者。曹操于建安年间权倾天下，在世时官至司空、大将军，自任丞相，爵至魏王，谥号武王。曹魏建立后其子曹丕追尊其为武皇帝，庙号太祖。",
			catelogy: "standard"
		},
		*/
		"rExtension_rEyd-WEI002": {
			translate: "司马懿",
			title: "通达权变",
			sex: "male",
			group: "wei",
			hp: 3,
			maxHp: 4,
			rank: { rarity: "epic" },
			replace: "simayi",
			skills: ["rExtension_winloud_anquan", "rExtension_winloud_taoyin"],
			tags: [
				"zhu",
				`ext:${translate}/res/image/character/rEyd-WEI002${dot}`,
				`died:ext:${translate}/res/audio/die/rEyd-WEI002.ogg`,
			],
			des: "字仲达，河内郡温县（今河南省焦作市温县）人，三国时期魏国权臣、政治家、军事家，亦是西晋的奠基人。曾抵御蜀汉丞相诸葛亮的北伐军，坚守疆土。历经曹操、曹丕、曹叡、曹芳四代君主，晚年发动高平陵之变，夺取曹魏的政权。是西晋开国皇帝晋武帝司马炎的祖父，曹魏后期两位权臣司马师和司马昭之父亲。",
			catelogy: "standard"
		},
		// 东吴
		"rExtension_rEyd-WU001": {
			translate: "孙权",
			title: "江东新杰",
			sex: "male",
			group: "wu",
			hp: 4,
			maxHp: 4,
			rank: { rarity: "epic" },
			replace: "sunquan",
			skills: ["rExtension_winloud_jiquan", "rExtension_winloud_jiquan_check", "rExtension_winloud_zhiheng"],
			tags: [
				"zhu",
				`ext:${translate}/res/image/character/rEyd-WU001${dot}`,
				`died:ext:${translate}/res/audio/die/rEyd-WU001.ogg`,
			],
			des: "字仲谋，东吴武烈皇帝孙坚第二个儿子、小霸王长沙桓王孙策之弟，吴郡富春县（今浙江省杭州市富阳区）人也，东汉末三国时期吴的著名政治家、战略家，同时是吴的缔造奠定者、建国皇帝，六朝开拓者之首。在位23年，享年70岁，谥号为大皇帝，庙号太祖。亦有一称作大皇。",
			catelogy: "standard"
		},
		// 群雄
		"rExtension_rEyd-QUN001": {
			translate: "袁术",
			title: "仲家帝",
			sex: "male",
			group: "qun",
			hp: 4,
			maxHp: 4,
			rank: { rarity: "epic" },
			replace: "yuanshu",
			skills: ["rExtension_winloud_yongsi", "rExtension_winloud_weidi"],
			tags: [
				"zhu",
				`ext:${translate}/res/image/character/rEyd-QUN001${dot}`,
				`died:ext:${translate}/res/audio/die/rEyd-QUN001.ogg`,
			],
			des: "字公路，汝南郡汝阳县（今河南商水）人，出身于官宦门阀，袁逢之子，袁基、袁绍之弟。东汉末年的军阀，官至左将军。最初以南阳作为领地，之后立足淮南僭号称帝，自立为仲氏政权，却得不到支持，反遭各方势力围攻，在位两年半屡次兵败，最终因悲愤吐血而死。 ",
			catelogy: "standard"
		},
	},
	skill: {
		// rEyd-SHU001 刘备
		"rExtension_winloud_hongyi": {
			name: "弘毅",
			des: "出牌阶段结束时和弃牌阶段结束时，你可将此阶段中场上角色进入弃牌堆的所有牌当作一张基本牌使用（无距离限制）",
			_oldes: "出牌阶段结束时和弃牌阶段结束时，你可将此阶段场上角色进入弃牌堆的所有牌当作一张本回合你未使用过的基本牌使用。",
			content: {
				audio: ["hongyi", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					player: ["phaseUseEnd", "phaseDiscardEnd"]
				},
				direct: true,
				filter: (event, player, _name) => {
					if (event.player != player || !player.isIn()) return false;
					const cards = game.players.concat(game.dead).reduce((cards, current) => {
						const historys = current.getHistory("lose", evt => {
							return evt.getParent(event.name) == event && evt.cards.filterInD("d").length > 0;
						});
						return cards.concat(historys.reduce((cards, history) => {
							return cards.concat(history.cards.filterInD("d"));
						}, Array.new()));
					}, Array.new());
					if (!cards.length) return false;
					// const historys = player.getHistory("useCard");
					const list = lib.inpile.reduce((result, name) => {
						if (name == "sha") {
							// if (historys.filter(evt => evt.card.name == "sha" && evt.skill == "rExtension_winloud_hongyi").length) return result;
							const list1 = (lib.filter.cardUsable({ name: "sha" }, player) && game.hasPlayer(current => player.canUse("sha", current))) ? [["基本", "", "sha"]] : Array.new();
							const list2 = lib.inpile_nature.reduce((result, name) => {
								return (lib.filter.cardUsable({ name: "sha", nature: name }, player) && game.hasPlayer(current => player.canUse({ name: "sha", nature: name }, current))) ? result.concat([["基本", "", "sha", name]]) : result;
							}, Array.new());
							return result.concat(list1, list2);
						}
						else {
							if (get.type(name) != "basic") return result;
							// if (historys.filter(evt => evt.card.name == name && evt.skill == "rExtension_winloud_hongyi").length) return result;
							return (lib.filter.cardUsable({ name }, player) && game.hasPlayer(current => player.canUse(name, current))) ? result.concat([["基本", "", name]]) : result;
						}
					}, Array.new());
					if (!list.length) return false;
					event.cards = cards;
					event.list = list;
					return true;
				},
				content: () => {
					"step 0"
					{
						player.chooseButton([get.prompt2(event.name), [trigger.list, "vcard"]]).set("ai", (button) => {
							const player = _status.event.player;
							const card = { name: button.link[2], nature: button.link[3] };
							if (card.name == "tao") {
								if (player.hp == 1 || (player.hp == 2 && !player.hasShan()) || player.needsToDiscard()) {
									return 5;
								}
								return 1;
							}
							if (card.name == "sha") {
								if (game.hasPlayer(function (current) {
									return player.canUse(card, current) && get.effect(current, card, player, player) > 0
								})) {
									if (card.nature == "fire") return 2.95;
									if (card.nature == "thunder" || card.nature == "ice") return 2.92;
									return 2.9;
								}
								return 0;
							}
							if (card.name == "jiu") {
								return 0.5;
							}
							return 0;
						});
					}
					"step 1"
					{
						if (result && result.bool && result.links[0]) {
							const card = { name: result.links[0][2], nature: result.links[0][3], isCard: true };
							player.chooseUseTarget(card, trigger.cards, true, "nodistance").set("logSkill", event.name);
						}
					}
				}
			}
		},
		"rExtension_winloud_renwang": {
			name: "仁望",
			des: "主公技，限定技，出牌阶段开始时，你可以令场上有手牌的其他角色选择一项：交给你一张牌，或对你使用一张无距离限制的【杀】 返日：你获得的牌不少于四张。",
			desc: "<b>主公技，限定技，</b>出牌阶段开始时，你可以令场上有手牌的其他角色选择一项：交给你一张牌，或对你使用一张无距离限制的【杀】<i> 返日：你获得的牌不少于四张。</i>",
			content: {
				audio: ["renwang", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					player: "phaseUseBegin"
				},
				unique: true,
				limited: true,
				zhuSkill: true,
				skillAnimation: true,
				animationStr: "仁望",
				animationColor: "fire",
				filter: (event, player, _name) => {
					if (player.storage["rExtension_winloud_renwang"]) return false;
					const players = game.filterPlayer(current => current != player && current.countCards("h") > 0);
					if (players.length) {
						event.targets = players.sortBySeat(player);
						return true;
					}
					return false;
				},
				init: (player, skill) => {
					player.storage[skill] = !player.checkZhuSkill(skill);
				},
				content: () => {
					"step 0"
					player.storage["rExtension_winloud_renwang"] = true;
					player.awakenSkill(event.name);
					event.num = 0;
					// player.addTempSkill("_rExtension_winloud_returnBack");
					"step 1"
					if (trigger.targets.length) {
						event.redo();
						const target = trigger.targets.shift();
						const next = game.createEvent("rExtension_winloud_renwang_choose");
						next.player = player;
						next.target = target;
						next.setContent(lib.skill[event.name].contentx);
					}
					"step 2"
					if (event.num >= 4) {
						player.logSkill("_rExtension_winloud_returnBack");
						game.log(`【${get.skillTranslation(event.name, player)}】`, "将于回合结束时重置")
						if (!player.storage["_rExtension_winloud_returnBack"]) player.storage["_rExtension_winloud_returnBack"] = Array.new();
						player.storage["_rExtension_winloud_returnBack"].push(event.name);
						if (!player.hasSkill("rExtension_winloud_returnBack_restore")) player.addTempSkill("rExtension_winloud_returnBack_restore");
					}
				},
				contentx: () => {
					"step 0"
					target.chooseToUse(`仁望：是否对${get.translation(player)}使用一张【杀】`, function (card, _player, _event) {
						if (get.name(card) != "sha") return false;
						return lib.filter.filterCard.apply(this, arguments);
					}).set("complexSelect", true)
						.set("filterTarget", function (_card, _player, target) {
							if (target != _status.event.sourcex && !ui.selected.targets.contains(_status.event.sourcex)) return false;
							return lib.filter.targetEnabled.apply(this, arguments);
						}).set("sourcex", player);
					"step 1"
					if (result.bool) event.finish();
					else target.chooseCard(`仁望：交给${get.translation(player)}一张牌`, true, "h", 1);
					"step 2"
					player.gain(result.cards, target, "give");
					++event.parent.num;
				},
				mark: true,
				intro: {
					content: "limited"
				}
			}
		},
		// rEyd-SHU003 张飞
		"rExtension_winloud_mangji": {
			name: "莽击",
			des: "当你使用【杀】对一名其他角色造成伤害后，你可以将X+1张牌当作【杀】对其使用（此杀不计入次数，X为本回合发动此技能的次数且最多为1）",
			content: {
				audio: ["mangji", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					source: "damageAfter",
				},
				locked: false,
				direct: true,
				filter: (event, player, _name) => event.player.isAlive() && event.card.name == "sha" && player.countCards("he") >= Math.min((player.getStat("rExtension_winloud_mangji") || 0) + 1, 2) && player.canUse({ name: "sha" }, event.player),
				content: () => {
					"step 0"
					const stat = player.getStat();
					if (!stat["rExtension_winloud_mangji"]) stat["rExtension_winloud_mangji"] = 0;
					++stat["rExtension_winloud_mangji"];
					"step 0"
					const next = player.chooseToUse().set("logSkill", event.name);
					next.set("prompt", get.prompt(event.name));
					next.set("prompt2", get.translation(`${event.name}_info`));
					next.set("addCount", false);
					// next.set("filterTarget", (...args) => lib.filter.filterTarget.apply(_status.event, args));
					next.set("_skill", event.name);
					next.set("_target", trigger.player);
					next.set("norestore", true);
					next.set("_backupevent", "rExtension_winloud_mangji_backup");
					next.set("custom", {
						add: {},
						replace: { window: (..._args) => undefined }
					});
					next.backup("rExtension_winloud_mangji_backup");
					"step 1"
					if (!result.bool) {
						const stat = player.getStat();
						--stat["rExtension_winloud_mangji"];
					};
				},
				mod: {
					targetInRange: (_card, _player, _target, now) => _status.event._skill == "rExtension_winloud_mangji" ? true : now
				}
			}
		},
		"rExtension_winloud_mangji_backup": {
			name: "莽击",
			content: {
				audio: false,
				filterCard: card => get.itemtype(card) == "card",
				position: "hes",
				filterTarget: (card, player, target) => target == _status.event._target && lib.filter.targetEnabled(card, player, target),
				selectCard: () => Math.min((_status.event.player.getStat("rExtension_winloud_mangji") || 0), 2),
				check: (card) => 6 - get.value(card),
				log: false,
				viewAs: {
					name: "sha"
				},
				precontent: function () {
					delete event.result.skill;
				},
			}
		},
		"rExtension_winloud_duanhe": {
			name: "断喝",
			des: "回合结束时，若你的手牌数为全场最少，你可以选择一名其他角色，然后其无法对你使用【杀】直至其回合结束。",
			content: {
				audio: ["duanhe", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					player: "phaseEnd"
				},
				locked: false,
				direct: true,
				filter: (_event, player, _name) => player.isMinHandcard(),
				content: () => {
					"step 0"
					player.chooseTarget(get.prompt2(event.name), lib.filter.notMe);
					"step 1"
					if (result.bool && result.targets && result.targets.length) {
						player.logSkill(event.name, result.targets);
						const target = result.targets[0];
						target.storage["rExtension_winloud_duanhe_mark"] = player;
						target.addTempSkill("rExtension_winloud_duanhe_mark", { player: "phaseEnd" });
					}
				},
				mod: {
					targetEnabled: (card, player, target, result) => (player.hasSkill("rExtension_winloud_duanhe_mark") && player.storage["rExtension_winloud_duanhe_mark"] == target && card.name == "sha") ? false : result
				}
			}
		},
		"rExtension_winloud_duanhe_mark": {
			name: "断喝",
			content: {
				onremove: true,
				charlotte: true,
				mark: "character",
				intro: {
					content: "无法对$使用【杀】直至回合结束",
				},
			}
		},
		// rEyd-SHU006 马超
		"rExtension_winloud_tieji": {
			name: "铁骑",
			des: "锁定技，若你进攻坐骑区有牌且防御坐骑区没有牌，你使用牌不可被响应。",
			desc: "<b>锁定技，</b>当你使用牌时若你进攻坐骑区有牌，且防御坐骑区没有牌，你使用牌不可被响应。",
			content: {
				audio: ["tieji", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					player: "useCard",
				},
				filter: (_event, player) => !player.getEquip(3) && player.getEquip(4),
				locked: true,
				forced: true,
				content: () => {
					trigger.directHit.addArray(game.players);
				},
			}
		},
		"rExtension_winloud_zhuiji": {
			name: "追击",
			des: "每回合限一次，当你造成伤害时，你可以使用一张牌（不计次数，无距离限制）",
			content: {
				audio: ["zhuiji", `ext:${translate}/res/audio/skill:2`],
				usable: 1,
				trigger: {
					source: "damageEnd",
				},
				locked: false,
				direct: true,
				filter: (event, player) => {
					const cards = player.getCards("hs", card => lib.filter.filterCard(card, player, event));
					if (!cards.length) return false;
					return event.canUsedCards = cards, true;
				},
				content: () => {
					"step 0"
					const next = player.chooseToUse((...args) => lib.filter.filterCard.apply(_status.event, args)).set("logSkill", event.name);
					next.set("prompt", get.prompt(event.name));
					next.set("prompt2", get.translation(`${event.name}_info`));
					next.set("addCount", false);
					next.set("filterTarget", (...args) => lib.filter.filterTarget.apply(_status.event, args));
					next.set("_skill", event.name);
					"step 1"
					if (!result.bool) {
						// --player.getStat("skill")["rExtension_winloud_tieji"];
						--player.storage.counttrigger[event.name];
					}
				},
				mod: {
					targetInRange: (_card, _player, _target, now) => _status.event._skill == "rExtension_winloud_zhuiji" ? true : now
				}
			}
		},
		// rEyd-WEI001 曹操

		// rEyd-WEI002 司马懿
		"rExtension_winloud_anquan": {
			name: "暗权",
			des: "每轮限一次，一名角色的判定阶段开始时，你可以摸一张牌，然后将一张牌置于牌堆顶；若如此做，本回合结束时，若当前回合角色跳过的回合数不为0，你获得其一张牌。",
			content: {
				audio: ["anquan", `ext:${translate}/res/audio/skill:2`],
				round: 1,
				trigger: {
					global: "phaseJudgeBegin"
				},
				preHidden: true,
				content: () => {
					"step 0"
					player.draw();
					"step 1"
					player.chooseCard("he", true, "将一张牌置于牌堆顶");
					"step 2"
					if (result && result.cards && result.cards.length) {
						const card = result.cards[0];
						player.lose(result.cards, ui.cardPile, "insert");
						game.log(player, "将", (get.position(card) == "h" ? "一张牌" : card), "置于牌堆顶");
						game.broadcastAll((player) => {
							var cardx = ui.create.card();
							cardx.classList.add("infohidden");
							cardx.classList.add("infoflip");
							player.$throw(cardx, 1000, "nobroadcast");
						}, player);
						player.addTempSkill("rExtension_winloud_anquan2");
					}
				},
			}
		},
		"rExtension_winloud_anquan2": {
			name: "暗权",
			content: {
				audio: ["anquan", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					global: "phaseEnd"
				},
				charlotte: true,
				forced: true,
				logTarget: "player",
				filter: (event, player, _name) => event.player != player && event.player.getHistory("skipped").length > 0,
				content: () => {
					player.gainPlayerCard(true, trigger.player, "he");
				},
			}
		},
		"rExtension_winloud_taoyin": {
			name: "韬隐",
			des: "锁定技，你无法使用本回合获得的牌；你的非本回合获得的牌不计入手牌上限。",
			content: {
				audio: ["taoyin", `ext:${translate}/res/audio/skill:2`],
				group: ["rExtension_winloud_taoyin_reset", "rExtension_winloud_taoyin_count", "rExtension_winloud_taoyin_discard"],
				mod: {
					cardEnabled2: (card, _player, result) => get.itemtype(card) == "card" && card.hasGaintag("rExtension_winloud_taoyin_gaintag") ? false : result,
					ignoredHandcard: (card, _player, result) => !card.hasGaintag("rExtension_winloud_taoyin_gaintag") ? true : result,
					cardDiscardable: (card, _player, name, result) => name == "phaseDiscard" && !card.hasGaintag("rExtension_winloud_taoyin_gaintag") ? false : result
				},
				onremove: (player) => {
					player.removeGaintag('fulin');
				},
			}
		},
		"rExtension_winloud_taoyin_reset": {
			name: "韬隐",
			content: {
				audio: false,
				trigger: {
					global: ["phaseBefore", "phaseAfter"]
				},
				charlotte: true,
				priority: 10,
				silent: true,
				filter: (_event, player, _name) => player.hasCards("h", card => card.hasGaintag("rExtension_winloud_taoyin_gaintag")),
				content: function () {
					player.removeGaintag("rExtension_winloud_taoyin_gaintag");
				}
			}
		},
		"rExtension_winloud_taoyin_count": {
			name: "韬隐",
			content: {
				audio: false,
				trigger: {
					player: "gainBegin"
				},
				charlotte: true,
				silent: true,
				content: function () {
					trigger.gaintag.add("rExtension_winloud_taoyin_gaintag");
				}
			}
		},
		"rExtension_winloud_taoyin_discard": {
			name: "韬隐",
			content: {
				audio: ["taoyin", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					player: "phaseDiscard"
				},
				charlotte: true,
				forced: true,
				firstDo: true,
				filter: (_event, player, _name) => player.getHistory("useCard", evt => evt.getParent("phaseUse")).length && player.needsToDiscard() > 0,
				content: () => {
					null
				}
			}
		},
		"rExtension_winloud_taoyin_gaintag": {
			name: "韬隐",
			content: {
				temp: true
			}
		},

		// rEyd-WU001 孙权
		"rExtension_winloud_jiquan": {
			name: "集权",
			des: "当其他角色的牌因弃置进入弃牌堆时，你可以弃置等量的牌，然后将本次获得的牌置于你的武将牌上，称作「权」；你可以将「权」如手牌般使用或打出。",
			content: {
				audio: ["jiquan", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					global: "loseAfter"
				},
				direct: true,
				init: (player, name) => {
					player.storage[name] = Array.new();
				},
				filter: (event, player, _name) => {
					if (event.player == player || event.type != "discard" || !event.cards.length || player.countCards("he") < event.cards.length) return false;
					const cards = event.cards.filter(card => get.position(card) == "d");
					return cards.length ? (event.gainCards = cards, true) : false;
				},
				content: () => {
					"step 0"
					const num = trigger.gainCards.length;
					player.chooseToDiscard(get.prompt2(event.name), num, "he").set("logSkill", event.name);
					"step 1"
					if (!result.bool) return event.finish();
					player.addToExpansion(trigger.gainCards, "gain2").gaintag.add(event.name);
					player.storage[event.name] = player.storage[event.name].concat(trigger.gainCards);
					"step 2"
					player.loseToSpecial(trigger.gainCards, event.name);
					"step 3"
					player.syncStorage(event.name);
					player.updateMarks();
				},
				sync: (player, skill) => {
					if (game.online) return;
					if (!player.storage[skill]) player.storage[skill] = Array.new();
					for (let i = 0; i < player.storage[skill].length; ++i) {
						if (get.position(player.storage[skill][i]) != "s") {
							player.storage[skill].splice(i--, 1);
						}
					}
					game.broadcast((player, skill, storage) => {
						player.storage[skill] = storage;
					}, player, skill, player.storage[skill]);
				},
				mark: true,
				marktext: "权",
				intro: {
					mark: (dialog, storage, player) => {
						if (storage && storage.length) {
							if (player.isUnderControl(true)) {
								dialog.addAuto(storage);
							}
							else {
								dialog.addText("共有" + get.cnNumber(storage.length) + "张「权」");
							}
						}
						else {
							dialog.addText("暂无「权」");
						}
					},
					content: (storage) => "共有" + get.cnNumber(storage.length) + "张「权」",
					markcount: (storage, _player) => storage ? storage.length : 0,
					onunmark: (storage, player) => {
						if (storage && storage.length) {
							player.$throw(storage, 1000);
							game.cardsDiscard(storage);
							game.log(storage.cards, "被置入了弃牌堆");
							storage.length = 0;
						}
					},
				},
			}
		},
		"rExtension_winloud_jiquan_check": {
			name: "集权",
			content: {
				audio: false,
				trigger: {
					player: "loseEnd",
				},
				charlotte: true,
				firstDo: true,
				silent: true,
				filter: (event, player) => {
					var skill = "rExtension_winloud_jiquan";
					if (!event.ss || !event.ss.length || event.parent.name == skill || event.parent.name == `${skill}2`) return false;
					if (!player.hasSkill(skill)) return false;
					return event.ss.filter((card) => player.storage[skill].includes(card)).length > 0;
				},
				content: function () {
					var skill = "rExtension_winloud_jiquan";
					player.storage[skill].removeArray(trigger.ss);
					lib.skill[skill].sync(player, skill);
					player.updateMarks();
				},
			}
		},
		"rExtension_winloud_zhiheng": {
			name: "制衡",
			des: "摸牌阶段开始时，若「权」的数量大于4，你可以弃置至多两张「权」并多摸等量的牌。",
			content: {
				audio: ["zhiheng", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					player: "phaseDrawBegin2",
				},
				direct: true,
				filter: (event, player, _name) => !event.numFixed && player.storage["rExtension_winloud_jiquan"].length > 4,
				content: () => {
					"step 0"
					player.chooseToDiscard(get.prompt2(event.name), [1, 2], "s", (card, player, _target) => player.storage["rExtension_winloud_jiquan"].includes(card)).set("logSkill", event.name);
					"step 1"
					if (result.bool) trigger.num += result.cards.length;
				}
			}
		},
		// rEyd-QUN001 袁术
		"rExtension_winloud_yongsi": {
			name: "庸肆",
			des: "锁定技，摸牌阶段，你多摸一张牌；当你即将受到其他角色造成的伤害时，若你的手牌数与体力值均大于其，此伤害+1",
			desc: "<b>锁定技，</b>摸牌阶段，你多摸一张牌；当你即将受到其他角色造成的伤害时，若你的手牌数与体力值均大于其，此伤害+1",
			content: {
				audio: ["yongsi", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					player: "phaseDrawBegin2"
				},
				locked: true,
				forced: true,
				preHidden: true,
				filter: (event, _player, _name) => !event.numFixed,
				content: () => {
					++trigger.num;
				},
				ai: {
					threaten: 1.5
				},
				group: "rExtension_winloud_yongsi2"
			}
		},
		"rExtension_winloud_yongsi2": {
			name: "庸肆",
			des: "锁定技，摸牌阶段，你多摸两张牌；当你即将受到其他角色造成的伤害时，若你的手牌数与体力值均大于其，此伤害+1。",
			desc: "<b>锁定技，</b>摸牌阶段，你多摸两张牌；当你即将受到其他角色造成的伤害时，若你的手牌数与体力值均大于其，此伤害+1。",
			content: {
				audio: ["yongsi2", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					player: "damageBegin3"
				},
				locked: true,
				forced: true,
				preHidden: true,
				filter: (event, player, _name) => event.source && event.source.isAlive() && player.hp > event.source.hp && player.countCards("h") > event.source.countCards("h"),
				content: () => {
					++trigger.num;
				},
				ai: {
					presha: true
				}
			}
		},
		"rExtension_winloud_weidi": {
			name: "伪帝",
			des: "锁定技，你的手牌上限+X；当一名群势力的其他角色成为【杀】的目标时，若你在其攻击范围内且你不为此杀的使用者，其可以弃置一张牌，将此【杀】转移给你（X为场上群势力角色数）",
			desc: "<b>锁定技，</b>你的手牌上限+X；当一名群势力的其他角色成为【杀】的目标时，若你在其攻击范围内且你不为此杀的使用者，其可以弃置一张牌，将此【杀】转移给你（X为场上群势力角色数）",
			content: {
				audio: ["weidi", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					global: "useCardToTarget"
				},
				locked: true,
				direct: true,
				logTarget: "target",
				filter: (event, player) => event.card.name == "sha" && event.player != player && !event.targets.contains(player) &&
					event.target.inRange(player) && event.target.countCards("he") > 0 && event.target.group == "qun",
				content: () => {
					"step 0"
					trigger.target.chooseCard("he", "是否对" + get.translation(player) + "发动【伪帝】？", "弃置一张牌，将" + get.translation(trigger.card) + "转移给" + get.translation(player)).set("ai", function (card) {
						if (!_status.event.check) return -1;
						return get.unuseful(card) + 9;
					}).set("check", function () {
						if (trigger.target.countCards("h", "shan")) {
							return -get.attitude(trigger.target, player);
						}
						if (get.attitude(trigger.target, player) < 5) {
							return 6 - get.attitude(trigger.target, player);
						}
						if (trigger.target.hp == 1 && player.countCards("h", "shan") == 0) {
							return 10 - get.attitude(trigger.target, player);
						}
						if (trigger.target.hp == 2 && player.countCards("h", "shan") == 0) {
							return 8 - get.attitude(trigger.target, player);
						}
						return -1;
					}() > 0);
					"step 1"
					if (result.bool) {
						trigger.target.logSkill("rExtension_winloud_weidi3", player);
						trigger.target.discard(result.cards);
						var evt = trigger.getParent();
						evt.triggeredTargets2.remove(trigger.target);
						evt.targets.remove(trigger.target);
						evt.targets.push(player);
					}
				},
				mod: {
					maxHandcard: (_player, num) => num + game.players.filter(current => current.group == "qun").length
				},
				group: "rExtension_winloud_weidi2"
			}
		},
		"rExtension_winloud_weidi2": {
			name: "伪帝",
			content: {
				audio: ["weidi", `ext:${translate}/res/audio/skill:2`],
				trigger: {
					player: "phaseDiscard"
				},
				charlotte: true,
				forced: true,
				firstDo: true,
				filter: (_event, player, _name) => player.countCards("h") > player.getHandcardLimit(),
				content: () => {
					null
				}
			}
		},
		"rExtension_winloud_weidi3": {
			name: "伪帝",
			content: {
				audio: ["weidi2", `ext:${translate}/res/audio/skill:2`]
			}
		}
	}
});  